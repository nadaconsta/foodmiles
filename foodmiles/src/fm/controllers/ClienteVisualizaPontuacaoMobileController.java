package fm.controllers;

import fm.entity.Cliente;
import fm.entity.Estabelecimento;

public interface ClienteVisualizaPontuacaoMobileController extends GenericController {

	Estabelecimento getEstabelecimento();

	String actionClose();

	void setEstabelecimento(Estabelecimento estabelecimento);

	Cliente getCliente();

	void setCliente(Cliente cliente);

	Integer getSaldoPontos();

}
