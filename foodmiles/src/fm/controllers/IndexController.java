package fm.controllers;

import fm.entity.Estabelecimento;
import fm.entity.Funcionario;

public interface IndexController extends GenericController {

	String actionLoginEstabelecimento();

	String actionLoginFuncionario();

	Estabelecimento getEstabelecimento();

	void setEstabelecimento(Estabelecimento estabelecimento);

	Funcionario getFuncionario();

	void setFuncionario(Funcionario funcionario);

	String getFuncionarioNumcpf();

	void setFuncionarioNumcpf(String numcpf);

	String getEstabelecimentoNumcnpj();

	void setEstabelecimentoNumcnpj(String strcnpj);

}