package fm.controllers;

import fm.managed.bean.GenericManagedBean;

public interface GenericController extends GenericManagedBean {

	void init();
	
	void actionClear();

	// This method should treat all foodmiles specific exceptions to a proper
	// message.
	void sendMessage(Exception e);

	String getPageName();

	void setPageName(String pageName);

}
