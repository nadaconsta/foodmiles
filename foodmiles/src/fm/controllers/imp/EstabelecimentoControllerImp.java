package fm.controllers.imp;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import fm.constants.Constants;
import fm.controllers.EstabelecimentoController;
import fm.controllers.EstabelecimentoFuncionarioController;
import fm.controllers.EstabelecimentoLancamentoController;
import fm.entity.Estabelecimento;
import fm.services.CreateService;
import fm.services.InstantiateService;
import fm.services.ReadService;

@SessionScoped
@ManagedBean(eager = true)
public class EstabelecimentoControllerImp extends GenericControllerImp
		implements EstabelecimentoController, Serializable {
	private static final long serialVersionUID = 9125787185682852500L;

	Estabelecimento estabelecimento;

	// Sub Controllers
	EstabelecimentoLancamentoController estabelecimentoLancamentoController;
	EstabelecimentoFuncionarioController estabelecimentoFuncionarioController;

	ReadService readService;
	CreateService createService;
	InstantiateService instantiateService;

	// Default page
	private final String defaultPage = Constants.PN_ESTABELECIMENTO_LANCAMENTO;

	@Override
	@PostConstruct
	public void init() {
		try {
			readService = super.findBean(ReadService.class);
			createService = super.findBean(CreateService.class);
			instantiateService = super.findBean(InstantiateService.class);

			estabelecimento = instantiateService.instantiateEstabelecimento(0l);

			this.setPageName(defaultPage);
		} catch (Exception e) {
			super.sendMessage(e);
		}
	}

	private void refresh() {
		try {
			readService.refreshEntityManager();
			estabelecimento = (Estabelecimento) readService
					.refresh(estabelecimento);
		} catch (Exception e) {
			super.sendMessage(e);
		}
	}

	private void close() {
		try {
			readService.refreshEntityManager();
			estabelecimento = instantiateService.instantiateEstabelecimento(0l);
		} catch (Exception e) {
			super.sendMessage(e);
		}
	}

	@Override
	public String actionLogout() {
		try {
			this.close();
			return getBeanName(IndexControllerImp.class);
		} catch (Exception e) {
			super.sendMessage(e);
		}
		return null;
	}

	@Override
	public String actionRefresh() {
		try {
			this.refresh();
			return getBeanName(this.getClass());
		} catch (Exception e) {
			super.sendMessage(e);
		}
		return null;
	}

	@Override
	public String redirectLancamento() {
		try {
			this.refresh();
			estabelecimentoLancamentoController = super
					.findBean(EstabelecimentoLancamentoController.class);
			estabelecimentoLancamentoController.actionClear();
			this.setPageName(Constants.PN_ESTABELECIMENTO_LANCAMENTO);
			return getBeanName(this.getClass());
		} catch (Exception e) {
			super.sendMessage(e);
		}
		return null;
	}

	@Override
	public String redirectFuncionario() {
		try {
			this.refresh();
			estabelecimentoFuncionarioController = super
					.findBean(EstabelecimentoFuncionarioController.class);
			estabelecimentoFuncionarioController.actionClear();
			this.setPageName(Constants.PN_ESTABELECIMENTO_FUNCIONARIO);
			return getBeanName(this.getClass());
		} catch (Exception e) {
			super.sendMessage(e);
		}
		return null;
	}

	@Override
	public Estabelecimento getEstabelecimento() {
		return this.estabelecimento;
	}

	@Override
	public void setEstabelecimento(Estabelecimento estabelecimento) {
		this.estabelecimento = estabelecimento;
	}

}
