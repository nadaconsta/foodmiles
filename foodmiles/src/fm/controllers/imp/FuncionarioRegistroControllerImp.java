package fm.controllers.imp;

import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.persistence.NoResultException;

import fm.constants.Constants;
import fm.controllers.FuncionarioController;
import fm.controllers.FuncionarioRegistroController;
import fm.entity.Cliente;
import fm.entity.Empresa;
import fm.entity.Funcionario;
import fm.entity.Pontuacao;
import fm.entity.RegistroPontuacao;
import fm.exceptions.ErrorException;
import fm.exceptions.InfoException;
import fm.exceptions.WarnException;
import fm.services.CalculateService;
import fm.services.CreateService;
import fm.services.FormatService;
import fm.services.GenerateService;
import fm.services.InstantiateService;
import fm.services.ReadService;
import fm.services.UpdateService;
import fm.services.ValidateService;

@SessionScoped
@ManagedBean(eager = true)
public class FuncionarioRegistroControllerImp extends GenericControllerImp
implements FuncionarioRegistroController, Serializable {
	private static final long serialVersionUID = 2011154662143393040L;
	Cliente cliente;
	RegistroPontuacao registroPontuacao;

	ReadService readService;
	CreateService createService;
	FormatService formatService;
	UpdateService updateService;
	GenerateService generateService;
	ValidateService validateService;
	CalculateService calculateService;
	InstantiateService instantiateService;

	// Parent Controller
	FuncionarioController funcionarioController;

	// Flag used to control the enabled/disabled action buttons
	private boolean actionRegisterEnabled;
	private boolean actionSubtractEnabled;
	private boolean actionCreditEnabled;
	private boolean noClientSearch;

	private int numRetirada;
	private int numCredito;

	@Override
	@PostConstruct
	public void init() {
		try {
			readService = super.findBean(ReadService.class);
			createService = super.findBean(CreateService.class);
			formatService = super.findBean(FormatService.class);
			updateService = super.findBean(UpdateService.class);
			generateService = super.findBean(GenerateService.class);
			validateService = super.findBean(ValidateService.class);
			calculateService = super.findBean(CalculateService.class);
			instantiateService = super.findBean(InstantiateService.class);

			funcionarioController = super.findBean(FuncionarioController.class);

			cliente = instantiateService.instantiateCliente(0l);
			registroPontuacao = instantiateService
					.instantiateRegistroPontuacao();

			actionRegisterEnabled = false;
			actionSubtractEnabled = false;
			actionCreditEnabled = false;
			numRetirada = Constants.ZERO_INT;
			numCredito = Constants.ZERO_INT;

		} catch (Exception e) {
			super.sendMessage(e);
		}
	}

	private void refresh() {
		try {
			funcionarioController.actionRefresh();
			cliente = (Cliente) readService.refresh(cliente);
		} catch (Exception e) {
			super.sendMessage(e);
		}
	}

	private void clear() {
		try {
			cliente = instantiateService.instantiateCliente(0l);
			registroPontuacao = instantiateService
					.instantiateRegistroPontuacao();

			actionRegisterEnabled = false;
			actionSubtractEnabled = false;
			actionCreditEnabled = false;
			numRetirada = Constants.ZERO_INT;
			numCredito = Constants.ZERO_INT;
		} catch (Exception e) {
			super.sendMessage(e);
		}
	}

	@Override
	public void actionClear() {
		try {
			this.clear();
		} catch (Exception e) {
			super.sendMessage(e);
		}
	}

	@Override
	public String actionSearch() {
		try {
			// Searches for the cliente by CPF
			boolean validated = true;
			if (!validateService.isValidCPF(cliente.getNumcpf())) {
				validated = false;
				super.sendMessage(new WarnException(550002));
			}
			if (validated) {
				numRetirada = Constants.ZERO_INT;
				numCredito = Constants.ZERO_INT;
				Long cpf = cliente.getNumcpf();

				registroPontuacao = instantiateService
						.instantiateRegistroPontuacao();
				if (cliente.getNumcpf() == Constants.ZERO_INT) {
					registroPontuacao.setFlggerarpontos(false);
					noClientSearch = true;
				} else {
					noClientSearch = false;
				}
				Cliente resultCliente = readService.readClienteByCPF(cpf);
				cliente = resultCliente;
				actionRegisterEnabled = true;
				actionSubtractEnabled = true;
				actionCreditEnabled = true;
				super.sendMessage(new InfoException(330003));
			}
		} catch (NoResultException e) {
			actionRegisterEnabled = true;
			actionSubtractEnabled = false;
			actionCreditEnabled = false;
			super.sendMessage(new InfoException(330002));
		} catch (Exception e) {
			super.sendMessage(e);
		}
		return null;
	}

	@Override
	public String actionRegister() {
		try {
			if (registroPontuacao.getValconta() > Constants.ZERO_INT) {
				registroPontuacao.setFuncionario(funcionarioController
						.getFuncionario());
				registroPontuacao.setEstabelecimento(funcionarioController
						.getFuncionario().getEstabelecimento());

				Pontuacao pontuacao = readService
						.readValidPontuacaoByIdEstabelecimento(funcionarioController
								.getFuncionario().getEstabelecimento()
								.getIdestabelecimento());
				Integer numPontos = calculateService.calcPontos(pontuacao,
						registroPontuacao);
				registroPontuacao.setNumpontos(numPontos);
				registroPontuacao.setDttinclusao(new Date());

				cliente.getLstRegistroPontuacao().add(registroPontuacao);
				funcionarioController.getFuncionario().getEstabelecimento()
				.getLstRegistroPontuacao().add(registroPontuacao);

				updateService.updateFuncionario(funcionarioController
						.getFuncionario());

				cliente = createService.createCliente(cliente);
				registroPontuacao.setCliente(cliente);

				createService.createRegistroPontuacao(registroPontuacao);

				this.refresh();

				actionSubtractEnabled = true;
				actionCreditEnabled = true;
				super.sendMessage(new InfoException(330004));
				return getBeanName(FuncionarioController.class);
			} else {
				super.sendMessage(new WarnException(550007));
			}
		} catch (NoResultException e) {
			super.sendMessage(new WarnException(550003));
		} catch (Exception e) {
			super.sendMessage(e);
		}
		return null;
	}

	@Override
	public String actionSubtract() {
		try {
			if (numRetirada > Constants.ZERO_INT) {
				int saldo = calculateService.calcSaldoPontos(cliente,
						funcionarioController.getFuncionario()
						.getEstabelecimento());
				if (numRetirada <= saldo) {
					registroPontuacao = instantiateService
							.instantiateRegistroPontuacao();
					registroPontuacao.setCliente(cliente);
					registroPontuacao.setFuncionario(funcionarioController
							.getFuncionario());
					registroPontuacao.setEstabelecimento(funcionarioController
							.getFuncionario().getEstabelecimento());
					registroPontuacao.setNumpontos(-numRetirada);
					cliente.getLstRegistroPontuacao().add(registroPontuacao);
					funcionarioController.getFuncionario().getEstabelecimento()
					.getLstRegistroPontuacao().add(registroPontuacao);

					createService.createRegistroPontuacao(registroPontuacao);
					updateService.updateCliente(cliente);
					updateService.updateFuncionario(funcionarioController
							.getFuncionario());

					this.refresh();

					super.sendMessage(new InfoException(330005));
				} else {
					super.sendMessage(new WarnException(550004));
				}
			} else {
				super.sendMessage(new WarnException(550005));
			}
			return getBeanName(FuncionarioController.class);
		} catch (Exception e) {
			super.sendMessage(e);
		}
		return null;
	}

	@Override
	public String actionCredit() {
		try {
			if (numCredito > Constants.ZERO_INT) {
				registroPontuacao = instantiateService
						.instantiateRegistroPontuacao();
				registroPontuacao.setCliente(cliente);
				registroPontuacao.setFuncionario(funcionarioController
						.getFuncionario());
				registroPontuacao.setEstabelecimento(funcionarioController
						.getFuncionario().getEstabelecimento());
				registroPontuacao.setNumpontos(numCredito);
				cliente.getLstRegistroPontuacao().add(registroPontuacao);
				funcionarioController.getFuncionario().getEstabelecimento()
				.getLstRegistroPontuacao().add(registroPontuacao);

				createService.createRegistroPontuacao(registroPontuacao);
				updateService.updateCliente(cliente);
				updateService.updateFuncionario(funcionarioController
						.getFuncionario());

				this.refresh();

				super.sendMessage(new InfoException(330012));
			} else {
				super.sendMessage(new WarnException(550015));
			}
			return getBeanName(FuncionarioController.class);
		} catch (Exception e) {
			super.sendMessage(e);
		}
		return null;
	}

	@Override
	public String actionReverse() {
		try {
			registroPontuacao.setFlgestornosolicitado(true);
			updateService.updateRegistroPontuacao(registroPontuacao);
			this.refresh();

			super.sendMessage(new InfoException(330008));

			return getBeanName(FuncionarioController.class);
		} catch (Exception e) {
			super.sendMessage(e);
		}
		return null;
	}

	@Override
	public List<String> listClienteCPFByPartialCPF(String value) {
		try {
			return readService.readListClienteCPFByPartialCPF(value);
		} catch (ParseException e) {
			super.sendMessage(new ErrorException(770002));
		} catch (Exception e) {
			super.sendMessage(e);
		}
		return new ArrayList<String>();
	}

	@Override
	public List<String> listClienteNomeSobrenomeByPartialNomeSobrenome(
			String value) {
		try {
			return readService
					.readListClienteNomeSobrenomeByPartialNomeSobrenome(value,
							funcionarioController.getFuncionario()
							.getEstabelecimento());
		} catch (ParseException e) {
			super.sendMessage(new ErrorException(770002));
		} catch (Exception e) {
			super.sendMessage(e);
		}
		return new ArrayList<String>();
	}

	@Override
	public List<Cliente> getListClienteByFrequency() {
		try {
			return generateService
					.generateListClienteByFrequency(funcionarioController
							.getFuncionario().getEstabelecimento());
		} catch (Exception e) {
			super.sendMessage(e);
		}
		return new ArrayList<Cliente>();
	}

	@Override
	public List<RegistroPontuacao> getListRegistroPontuacaoRecenteByFuncionario() {
		try {
			return generateService
					.generateListRegistroPontuacaoRecenteByFuncionario(funcionarioController
							.getFuncionario());
		} catch (Exception e) {
			super.sendMessage(e);
		}
		return new ArrayList<RegistroPontuacao>();
	}

	@Override
	public String getClienteNumcpf() {
		try {
			Long numcpf = cliente.getNumcpf();
			if (numcpf.intValue() == Constants.ZERO_INT) {
				return null;
			}
			String cpf = formatService.fillLeft(numcpf.toString(),
					Constants.ZERO_STRING, Constants.CPF_SIZE);
			return formatService.formatString(cpf, Constants.MASK_CPF);
		} catch (ParseException e) {
			super.sendMessage(new ErrorException(770002));
		} catch (Exception e) {
			super.sendMessage(e);
		}
		return null;
	}

	@Override
	public void setClienteNumcpf(String strcpf) {
		try {
			cliente = instantiateService.instantiateCliente(0l);
			cliente.setNumcpf(formatService.formatCPFLong(strcpf));
		} catch (Exception e) {
			super.sendMessage(e);
		}
	}

	@Override
	public String getClienteNomeSobrenome() {
		return null;
	}

	@Override
	public void setClienteNomeSobrenome(String nomeSobrenome) {
		try {
			String strcpf = formatService.extractBracketedValue(nomeSobrenome);
			cliente = instantiateService.instantiateCliente(0l);
			cliente.setNumcpf(formatService.formatCPFLong(strcpf));
		} catch (IndexOutOfBoundsException e) {
			super.sendMessage(new WarnException(550006));
		} catch (Exception e) {
			super.sendMessage(e);
		}
	}

	@Override
	public String getClienteEmpresa() {
		try {
			return generateService
					.generateNomeEmpresaCliente(cliente, funcionarioController
							.getFuncionario().getEstabelecimento());
		} catch (Exception e) {
			super.sendMessage(e);
		}
		return null;
	}

	@Override
	public void setClienteEmpresa(String nomeEmpresa) {
		try {
			List<Empresa> lstEmpresaEstabelecimento = funcionarioController
					.getFuncionario().getEstabelecimento().getLstEmpresa();
			boolean exists = false;
			for (Empresa empresaEstabelecimento : lstEmpresaEstabelecimento) {
				if (empresaEstabelecimento.getStrnome().equalsIgnoreCase(
						nomeEmpresa)) {
					if (!cliente.getLstEmpresa().contains(
							empresaEstabelecimento)) {
						List<Empresa> lstEmpresaCliente = cliente
								.getLstEmpresa();
						lstEmpresaCliente.removeAll(lstEmpresaEstabelecimento);
						lstEmpresaCliente.add(empresaEstabelecimento);
						cliente.setLstEmpresa(lstEmpresaCliente);
					}
					exists = true;
				}
			}
			if (!exists) {
				Empresa empresa;
				try {
					empresa = readService.readEmpresaByNome(nomeEmpresa);
				} catch (NoResultException e) {
					empresa = createService.createEmpresa(nomeEmpresa);
				}

				lstEmpresaEstabelecimento.add(empresa);
				funcionarioController.getFuncionario().getEstabelecimento()
				.setLstEmpresa(lstEmpresaEstabelecimento);

				if (!cliente.getLstEmpresa().contains(empresa)) {
					List<Empresa> lstEmpresaCliente = cliente.getLstEmpresa();
					lstEmpresaCliente.removeAll(lstEmpresaEstabelecimento);
					lstEmpresaCliente.add(empresa);
					cliente.setLstEmpresa(lstEmpresaCliente);
				}
			}
		} catch (Exception e) {
			super.sendMessage(e);
		}
	}

	@Override
	public Integer getVisitaRecente() {
		try {
			return calculateService
					.calcMediaVisitas(cliente, funcionarioController
							.getFuncionario().getEstabelecimento(), true);
		} catch (Exception e) {
			super.sendMessage(e);
		}
		return null;
	}

	@Override
	public Integer getVisitaTotal() {
		try {
			return calculateService
					.calcMediaVisitas(cliente, funcionarioController
							.getFuncionario().getEstabelecimento(), false);
		} catch (Exception e) {
			super.sendMessage(e);
		}
		return null;
	}

	@Override
	public Date getVisitaUltima() {
		try {
			return calculateService
					.calcUltimaVisita(cliente, funcionarioController
							.getFuncionario().getEstabelecimento());
		} catch (Exception e) {
			super.sendMessage(e);
		}
		return null;
	}

	@Override
	public Integer getSaldoPontos() {
		try {
			return calculateService
					.calcSaldoPontos(cliente, funcionarioController
							.getFuncionario().getEstabelecimento());
		} catch (Exception e) {
			super.sendMessage(e);
		}
		return null;
	}

	@Override
	public Integer getSaldoAposRetirada() {
		try {
			return calculateService
					.calcSaldoPontos(cliente, funcionarioController
							.getFuncionario().getEstabelecimento())
					- numRetirada;
		} catch (Exception e) {
			super.sendMessage(e);
		}
		return null;
	}

	@Override
	public Integer getSaldoAposCredito() {
		try {
			return calculateService
					.calcSaldoPontos(cliente, funcionarioController
							.getFuncionario().getEstabelecimento())
					+ numCredito;
		} catch (Exception e) {
			super.sendMessage(e);
		}
		return null;
	}

	@Override
	public int getNumRetirada() {
		return numRetirada;
	}

	@Override
	public void setNumRetirada(int numRetirada) {
		this.numRetirada = numRetirada;
	}

	@Override
	public int getNumCredito() {
		return numCredito;
	}

	@Override
	public void setNumCredito(int numCredito) {
		this.numCredito = numCredito;
	}

	@Override
	public Funcionario getFuncionario() {
		return funcionarioController.getFuncionario();
	}

	@Override
	public Cliente getCliente() {
		return cliente;
	}

	@Override
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	@Override
	public RegistroPontuacao getRegistroPontuacao() {
		return registroPontuacao;
	}

	@Override
	public void setRegistroPontuacao(RegistroPontuacao registroPontuacao) {
		this.registroPontuacao = registroPontuacao;
	}

	@Override
	public boolean isActionRegisterEnabled() {
		return actionRegisterEnabled;
	}

	@Override
	public boolean isActionSubtractEnabled() {
		return actionSubtractEnabled;
	}

	@Override
	public boolean isActionCreditEnabled() {
		return actionCreditEnabled;
	}

	@Override
	public boolean isNoClientSearch() {
		return noClientSearch;
	}
}