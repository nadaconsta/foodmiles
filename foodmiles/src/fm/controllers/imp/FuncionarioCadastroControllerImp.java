package fm.controllers.imp;

import java.io.Serializable;
import java.text.ParseException;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import fm.constants.Constants;
import fm.controllers.FuncionarioCadastroController;
import fm.controllers.FuncionarioController;
import fm.entity.Funcionario;
import fm.exceptions.ErrorException;
import fm.exceptions.InfoException;
import fm.exceptions.WarnException;
import fm.services.FormatService;
import fm.services.UpdateService;
import fm.services.ValidateService;

@SessionScoped
@ManagedBean(eager = true)
public class FuncionarioCadastroControllerImp extends GenericControllerImp
		implements FuncionarioCadastroController, Serializable {
	private static final long serialVersionUID = 2117817027269476264L;

	// Parent Controller
	FuncionarioController funcionarioController;

	UpdateService updateService;
	FormatService formatService;
	ValidateService validateService;

	@Override
	@PostConstruct
	public void init() {
		updateService = super.findBean(UpdateService.class);
		formatService = super.findBean(FormatService.class);
		validateService = super.findBean(ValidateService.class);

		funcionarioController = super.findBean(FuncionarioController.class);
	}

	private void refresh() {
		try {
			funcionarioController.actionRefresh();
		} catch (Exception e) {
			super.sendMessage(e);
		}
	}

	@Override
	public void actionClear() {
		// DO NOTHING
	}

	@Override
	public String actionSave() {
		try {
			// Validation methods for funcionario
			boolean validated = true;
			if (!validateService.isValidEmail(funcionarioController
					.getFuncionario().getStremail())) {
				super.sendMessage(new WarnException(550009));
				validated = false;
			}
			if (!validateService.isValidCelular(funcionarioController
					.getFuncionario().getNumdddcelular(), funcionarioController
					.getFuncionario().getNumcelular())) {
				super.sendMessage(new WarnException(550010));
				validated = false;
			}
			if (!validateService.isValidTelefone(funcionarioController
					.getFuncionario().getNumdddtelefone(),
					funcionarioController.getFuncionario().getNumtelefone())) {
				super.sendMessage(new WarnException(550011));
				validated = false;
			}
			if (!validateService.confirmPassword(funcionarioController
					.getFuncionario().getStrsenha(), funcionarioController
					.getFuncionario().getDescription())) {
				super.sendMessage(new WarnException(550012));
				validated = false;
			}
			if (!validateService.isValidPassword(funcionarioController
					.getFuncionario().getStrsenha())) {
				super.sendMessage(new WarnException(550013));
				validated = false;
			}
			if (!validateService.isValidCPF(funcionarioController
					.getFuncionario().getNumcpf())) {
				super.sendMessage(new WarnException(550002));
				validated = false;
			}
			if (validated) {
				funcionarioController.getFuncionario().setStrsenha(
						validateService.encryptPassword(funcionarioController
								.getFuncionario().getStrsenha()));
				funcionarioController.unblockPasswordChange();

				updateService.updateFuncionario(funcionarioController
						.getFuncionario());

				this.refresh();
				super.sendMessage(new InfoException(330001));
				return getBeanName(FuncionarioController.class);
			}
		} catch (Exception e) {
			super.sendMessage(e);
		}
		return null;
	}

	@Override
	public String getFuncionarioNumcpf() {
		try {
			Long numcpf = funcionarioController.getFuncionario().getNumcpf();
			if (numcpf.intValue() == Constants.ZERO_INT) {
				return null;
			}
			String cpf = formatService.fillLeft(numcpf.toString(),
					Constants.ZERO_STRING, Constants.CPF_SIZE);
			return formatService.formatString(cpf, Constants.MASK_CPF);
		} catch (ParseException e) {
			super.sendMessage(new ErrorException(770002));
		} catch (Exception e) {
			super.sendMessage(e);
		}
		return null;
	}

	@Override
	public void setFuncionarioNumcpf(String strcpf) {
		try {
			funcionarioController.getFuncionario().setNumcpf(
					formatService.formatCPFLong(strcpf));
		} catch (Exception e) {
			super.sendMessage(e);
		}
	}

	@Override
	public String getfuncionarioTelefone() {
		try {
			return (String.valueOf(funcionarioController.getFuncionario()
					.getNumdddtelefone()) + String
					.valueOf(funcionarioController.getFuncionario()
							.getNumtelefone()));
		} catch (Exception e) {
			super.sendMessage(e);
		}
		return null;
	}

	@Override
	public void setfuncionarioTelefone(String strTelefone) {
		try {
			funcionarioController.getFuncionario().setNumdddtelefone(
					formatService.extractDDD(strTelefone));
			funcionarioController.getFuncionario().setNumtelefone(
					formatService.extractTelefone(strTelefone));
		} catch (Exception e) {
			super.sendMessage(e);
		}
	}

	@Override
	public String getfuncionarioCelular() {
		try {
			return (String.valueOf(funcionarioController.getFuncionario()
					.getNumdddcelular()) + String.valueOf(funcionarioController
					.getFuncionario().getNumcelular()));
		} catch (Exception e) {
			super.sendMessage(e);
		}
		return null;
	}

	@Override
	public void setfuncionarioCelular(String strCelular) {
		try {
			funcionarioController.getFuncionario().setNumdddcelular(
					formatService.extractDDD(strCelular));
			funcionarioController.getFuncionario().setNumcelular(
					formatService.extractCelular(strCelular));
		} catch (Exception e) {
			super.sendMessage(e);
		}
	}

	@Override
	public Funcionario getFuncionario() {
		return funcionarioController.getFuncionario();
	}

}
