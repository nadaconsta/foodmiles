package fm.controllers.imp;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import fm.controllers.ClienteConsultaPontuacaoMobileController;
import fm.controllers.ClienteVisualizaPontuacaoMobileController;
import fm.entity.Cliente;
import fm.entity.Estabelecimento;
import fm.services.CalculateService;
import fm.services.InstantiateService;

@SessionScoped
@ManagedBean(eager = true)
public class ClienteVisualizaPontuacaoMobileControllerImp extends GenericControllerImp
		implements ClienteVisualizaPontuacaoMobileController, Serializable {
	private static final long serialVersionUID = 3324814355910218837L;
	
	Cliente cliente;
	Estabelecimento estabelecimento;

	CalculateService calculateService;
	InstantiateService instantiateService;
	
	// Parent Controller
	ClienteConsultaPontuacaoMobileController clienteConsultaPontuacaoMobileController;

	@Override
	@PostConstruct
	public void init() {
		try {
			calculateService = super.findBean(CalculateService.class);
			instantiateService = super.findBean(InstantiateService.class);
			
			clienteConsultaPontuacaoMobileController = super.findBean(ClienteConsultaPontuacaoMobileController.class);
			
			cliente = instantiateService.instantiateCliente(0l);
			estabelecimento = instantiateService.instantiateEstabelecimento(0l);
			
			this.setMobile(true);
		} catch (Exception e) {
			super.sendMessage(e);
		}
	}

	@Override
	public Estabelecimento getEstabelecimento() {
		return estabelecimento;
	}

	@Override
	public void setEstabelecimento(Estabelecimento estabelecimento) {
		this.estabelecimento = estabelecimento;
	}
	
	@Override
	public String actionClose() {
		return null;
	}
	
	@Override
	public Integer getSaldoPontos() {
		try {
			return calculateService
					.calcSaldoPontos(cliente, estabelecimento);
		} catch (Exception e) {
			super.sendMessage(e);
		}
		return null;
	}

	@Override
	public Cliente getCliente() {
		return cliente;
	}

	@Override
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

}
