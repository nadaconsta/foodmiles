package fm.controllers.imp;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.persistence.NoResultException;

import fm.controllers.EstabelecimentoController;
import fm.controllers.FuncionarioController;
import fm.controllers.IndexController;
import fm.entity.Estabelecimento;
import fm.entity.Funcionario;
import fm.exceptions.WarnException;
import fm.services.CreateService;
import fm.services.FormatService;
import fm.services.InstantiateService;
import fm.services.ReadService;
import fm.services.ValidateService;

@ViewScoped
@ManagedBean(eager = true)
public class IndexControllerImp extends GenericControllerImp implements
IndexController, Serializable {
	private static final long serialVersionUID = -5674016181727311219L;
	
	Funcionario funcionario;
	Estabelecimento estabelecimento;

	// Sub Controllers
	FuncionarioController funcionarioController;
	EstabelecimentoController estabelecimentoController;

	ReadService readService;
	CreateService createService;
	FormatService formatService;
	ValidateService validateService;
	InstantiateService instantiateService;

	@Override
	@PostConstruct
	public void init() {
		try {
			readService = super.findBean(ReadService.class);
			createService = super.findBean(CreateService.class);
			formatService = super.findBean(FormatService.class);
			validateService = super.findBean(ValidateService.class);
			instantiateService = super.findBean(InstantiateService.class);

			funcionarioController = super.findBean(FuncionarioController.class);
			estabelecimentoController = super.findBean(EstabelecimentoController.class);

			readService.refreshEntityManager();

			funcionario = instantiateService.instantiateFuncionario(0l);
			estabelecimento = instantiateService.instantiateEstabelecimento(0l);
			funcionario.setEstabelecimento(estabelecimento);
		} catch (Exception e) {
			super.sendMessage(e);
		}
	}

	@Override
	public String actionLoginEstabelecimento() {
		try {
			boolean validated = true;
			if (!validateService.isValidCNPJ(estabelecimento.getNumcnpj())) {
				validated = false;
				super.sendMessage(new WarnException(550002));
			}
			if (validated) {
				// TODO Primeiro login e criptografia de senha.
				estabelecimento = readService.readEstabelecimentoByCNPJSenha(
						estabelecimento.getNumcnpj(),
						estabelecimento.getStrsenha());

				estabelecimentoController.setEstabelecimento(estabelecimento);
				return estabelecimentoController.getBeanName();
			}
		} catch (NoResultException e) {
			super.sendMessage(new WarnException(550001));
		} catch (Exception e) {
			super.sendMessage(e);
		}
		return this.getBeanName();
	}

	@Override
	public String actionLoginFuncionario() {
		try {
			boolean validated = true;
			if (!validateService.isValidCPF(funcionario.getNumcpf())) {
				validated = false;
				super.sendMessage(new WarnException(550002));
			}
			if (validated) {
				if (this.funcionario.getStrsenha().isEmpty()) {
					funcionarioController.setForcePasswordChange(true);
					funcionario = readService
							.readFuncionarioByCPFSenhaNull(funcionario
									.getNumcpf());
				} else {
					funcionarioController.setForcePasswordChange(false);
					this.funcionario.setStrsenha(validateService
							.encryptPassword(this.funcionario.getStrsenha()));
					funcionario = readService.readFuncionarioByCPFSenha(
							funcionario.getNumcpf(), funcionario.getStrsenha());
				}
			}

			if (funcionario.isFlgativo()) {
				funcionarioController.setFuncionario(funcionario);
				return funcionarioController.getBeanName();
			} else {
				super.sendMessage(new WarnException(550008));
			}
		} catch (NoResultException e) {
			super.sendMessage(new WarnException(550001));
		} catch (Exception e) {
			super.sendMessage(e);
		}
		return this.getBeanName();
	}

	@Override
	public void setFuncionarioNumcpf(String strcpf) {
		try {
			this.funcionario.setNumcpf(formatService.formatCPFLong(strcpf));
		} catch (Exception e) {
			super.sendMessage(e);
		}
	}

	@Override
	public String getFuncionarioNumcpf() {
		try {
			return formatService.formatCPFString(this.funcionario.getNumcpf());
		} catch (Exception e) {
			super.sendMessage(e);
		}
		return null;
	}

	@Override
	public void setEstabelecimentoNumcnpj(String strcnpj) {
		try {
			this.estabelecimento.setNumcnpj(formatService
					.formatCNPJLong(strcnpj));
		} catch (Exception e) {
			super.sendMessage(e);
		}
	}

	@Override
	public String getEstabelecimentoNumcnpj() {
		try {
			return formatService.formatCNPJString(this.estabelecimento
					.getNumcnpj());
		} catch (Exception e) {
			super.sendMessage(e);
		}
		return null;
	}

	@Override
	public Estabelecimento getEstabelecimento() {
		return estabelecimento;
	}

	@Override
	public void setEstabelecimento(Estabelecimento estabelecimento) {
		this.estabelecimento = estabelecimento;
	}

	@Override
	public Funcionario getFuncionario() {
		return funcionario;
	}

	@Override
	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}
}
