package fm.controllers.imp;

import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import fm.constants.Constants;
import fm.controllers.EstabelecimentoController;
import fm.controllers.EstabelecimentoFuncionarioController;
import fm.entity.Estabelecimento;
import fm.entity.Funcionario;
import fm.exceptions.ErrorException;
import fm.exceptions.InfoException;
import fm.exceptions.WarnException;
import fm.services.FormatService;
import fm.services.GenerateService;
import fm.services.InstantiateService;
import fm.services.UpdateService;
import fm.services.ValidateService;

@SessionScoped
@ManagedBean(eager = true)
public class EstabelecimentoFuncionarioControllerImp extends
		GenericControllerImp implements EstabelecimentoFuncionarioController,
		Serializable {
	private static final long serialVersionUID = -8820649166746794235L;

	Funcionario funcionario;

	// Parent Controller
	EstabelecimentoController estabelecimentoController;

	UpdateService updateService;
	FormatService formatService;
	GenerateService generateService;
	ValidateService validateService;
	InstantiateService instantiateService;

	// Flag used to control the enabled/disabled action buttons
	private boolean actionResetPasswordEnabled;

	@Override
	@PostConstruct
	public void init() {
		updateService = super.findBean(UpdateService.class);
		formatService = super.findBean(FormatService.class);
		generateService = super.findBean(GenerateService.class);
		validateService = super.findBean(ValidateService.class);
		instantiateService = super.findBean(InstantiateService.class);

		estabelecimentoController = super
				.findBean(EstabelecimentoController.class);
		actionResetPasswordEnabled = false;

		funcionario = instantiateService.instantiateFuncionario(0l);
	}

	private void refresh() {
		try {
			estabelecimentoController.actionRefresh();
		} catch (Exception e) {
			super.sendMessage(e);
		}
	}

	private void clear() {
		try {
			actionResetPasswordEnabled = false;
			funcionario = instantiateService.instantiateFuncionario(0l);
		} catch (Exception e) {
			super.sendMessage(e);
		}
	}

	@Override
	public void actionClear() {
		try {
			this.clear();
		} catch (Exception e) {
			super.sendMessage(e);
		}
	}

	@Override
	public Estabelecimento getEstabelecimento() {
		try {
			return estabelecimentoController.getEstabelecimento();
		} catch (Exception e) {
			super.sendMessage(e);
		}
		return null;
	}

	@Override
	public String actionCreate() {
		try {
			this.clear();
			super.sendMessage(new InfoException(330010));
			return getBeanName(EstabelecimentoController.class);
		} catch (Exception e) {
			super.sendMessage(e);
		}
		return null;
	}

	@Override
	public String actionSave() {
		try {
			// Validation methods for funcionario
			boolean validated = true;
			if (!validateService.isValidEmail(funcionario.getStremail())) {
				super.sendMessage(new WarnException(550009));
				validated = false;
			}
			if (!validateService.isValidCelular(funcionario.getNumdddcelular(),
					funcionario.getNumcelular())) {
				super.sendMessage(new WarnException(550010));
				validated = false;
			}
			if (!validateService.isValidTelefone(
					funcionario.getNumdddtelefone(),
					funcionario.getNumtelefone())) {
				super.sendMessage(new WarnException(550011));
				validated = false;
			}
			if (!validateService.isValidCPF(funcionario.getNumcpf())) {
				super.sendMessage(new WarnException(550002));
				validated = false;
			}
			if (validated) {
				funcionario.setEstabelecimento(estabelecimentoController
						.getEstabelecimento());
				updateService.updateFuncionario(funcionario);

				this.refresh();
				super.sendMessage(new InfoException(330009));
				return getBeanName(EstabelecimentoController.class);
			}
			this.refresh();
		} catch (Exception e) {
			super.sendMessage(e);
		}
		return null;
	}

	@Override
	public String actionResetPassword() {
		try {
			//TODO Validation methods for funcionario
			boolean validated = true;
			if (validated) {
				funcionario.setStrsenha(null);
				updateService.updateFuncionario(funcionario);

				super.sendMessage(new InfoException(330011));
				return getBeanName(EstabelecimentoController.class);
			}
		} catch (Exception e) {
			super.sendMessage(e);
		}
		return null;
	}

	@Override
	public List<Funcionario> getListFuncionarioByEstabelecimento() {
		try {
			return generateService
					.generateListFuncionarioByEstabelecimento(estabelecimentoController
							.getEstabelecimento());
		} catch (Exception e) {
			super.sendMessage(e);
		}
		return new ArrayList<Funcionario>();
	}

	@Override
	public String getFuncionarioNumcpf() {
		try {
			Long numcpf = funcionario.getNumcpf();
			if (numcpf.intValue() == Constants.ZERO_INT) {
				return null;
			}
			String cpf = formatService.fillLeft(numcpf.toString(),
					Constants.ZERO_STRING, Constants.CPF_SIZE);
			return formatService.formatString(cpf, Constants.MASK_CPF);
		} catch (ParseException e) {
			super.sendMessage(new ErrorException(770002));
		} catch (Exception e) {
			super.sendMessage(e);
		}
		return null;
	}

	@Override
	public void setFuncionarioNumcpf(String strcpf) {
		try {
			funcionario.setNumcpf(formatService.formatCPFLong(strcpf));
		} catch (Exception e) {
			super.sendMessage(e);
		}
	}

	@Override
	public String getfuncionarioTelefone() {
		try {
			return (String.valueOf(funcionario.getNumdddtelefone()) + String
					.valueOf(funcionario.getNumtelefone()));
		} catch (Exception e) {
			super.sendMessage(e);
		}
		return null;
	}

	@Override
	public void setfuncionarioTelefone(String strTelefone) {
		try {
			funcionario
					.setNumdddtelefone(formatService.extractDDD(strTelefone));
			funcionario.setNumtelefone(formatService
					.extractTelefone(strTelefone));
		} catch (Exception e) {
			super.sendMessage(e);
		}
	}

	@Override
	public String getfuncionarioCelular() {
		try {
			return (String.valueOf(funcionario.getNumdddcelular()) + String
					.valueOf(funcionario.getNumcelular()));
		} catch (Exception e) {
			super.sendMessage(e);
		}
		return null;
	}

	@Override
	public void setfuncionarioCelular(String strCelular) {
		try {
			funcionario.setNumdddcelular(formatService.extractDDD(strCelular));
			funcionario.setNumcelular(formatService.extractCelular(strCelular));
		} catch (Exception e) {
			super.sendMessage(e);
		}
	}

	@Override
	public Funcionario getFuncionario() {
		return funcionario;
	}

	@Override
	public void setFuncionario(Funcionario funcionario) {
		actionResetPasswordEnabled = true;
		this.funcionario = funcionario;
	}

	@Override
	public boolean isActionResetPasswordEnabled() {
		return actionResetPasswordEnabled;
	}

}