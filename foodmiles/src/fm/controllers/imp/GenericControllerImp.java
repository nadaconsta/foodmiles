package fm.controllers.imp;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import fm.constants.Constants;
import fm.controllers.GenericController;
import fm.exceptions.ErrorException;
import fm.exceptions.InfoException;
import fm.exceptions.WarnException;
import fm.managed.bean.imp.GenericManagedBeanImp;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

@SessionScoped
@ManagedBean(eager = true)
public class GenericControllerImp extends GenericManagedBeanImp implements
		GenericController {

	private String pageName;

	@Override
	public void init() {
		throw new NotImplementedException();
	}

	@Override
	public void actionClear() {
		throw new NotImplementedException();
	}

	@Override
	public void sendMessage(Exception e) {
		if (InfoException.class.equals(e.getClass())) {
			InfoException me = (InfoException) e;
			FacesContext.getCurrentInstance().addMessage(
					null,
					new FacesMessage(FacesMessage.SEVERITY_INFO, me.getCode()
							.toString(), me.getMessage()));
		} else if (WarnException.class.equals(e.getClass())) {
			WarnException me = (WarnException) e;
			FacesContext.getCurrentInstance().addMessage(
					null,
					new FacesMessage(FacesMessage.SEVERITY_WARN, me.getCode()
							.toString(), me.getMessage()));
		} else if (ErrorException.class.equals(e.getClass())) {
			ErrorException ee = (ErrorException) e;
			FacesContext.getCurrentInstance().addMessage(
					null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, ee.getCode()
							.toString(), ee.getMessage()));
		} else {
			e.printStackTrace();
			String stack = e.getMessage() + Constants.DEFAULT_SEPARATOR;
			for (StackTraceElement ste : e.getStackTrace()) {
				stack += ste.toString() + Constants.DEFAULT_SEPARATOR;
			}
			ErrorException ee = new ErrorException(Constants.FATAL_CODE);

			FacesContext.getCurrentInstance().addMessage(
					null,
					new FacesMessage(FacesMessage.SEVERITY_FATAL, ee.getCode()
							.toString(), ee.getMessage()
							+ Constants.DEFAULT_SEPARATOR + stack));
		}
	}

	@Override
	public String getPageName() {
		return pageName;
	}

	@Override
	public void setPageName(String pageName) {
		this.pageName = pageName;
	}
}
