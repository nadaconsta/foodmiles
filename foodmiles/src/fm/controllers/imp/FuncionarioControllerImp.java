package fm.controllers.imp;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import fm.constants.Constants;
import fm.controllers.FuncionarioCadastroController;
import fm.controllers.FuncionarioController;
import fm.controllers.FuncionarioRegistroController;
import fm.entity.Funcionario;
import fm.exceptions.WarnException;
import fm.services.InstantiateService;
import fm.services.ReadService;

@SessionScoped
@ManagedBean(eager = true)
public class FuncionarioControllerImp extends GenericControllerImp implements
		FuncionarioController, Serializable {
	private static final long serialVersionUID = 101155606146112249L;

	Funcionario funcionario;

	// Sub Controllers
	FuncionarioRegistroController funcionarioRegistroController;
	FuncionarioCadastroController funcionarioCadastroController;

	ReadService readService;
	InstantiateService instantiateService;

	// Flag used to control the enabled/disabled action buttons
	private boolean forcePasswordChange;

	// Default page
	private final String defaultPage = Constants.PN_FUNCIONARIO_REGISTRO;

	@Override
	@PostConstruct
	public void init() {
		try {
			readService = super.findBean(ReadService.class);
			instantiateService = super.findBean(InstantiateService.class);

			funcionario = instantiateService.instantiateFuncionario(0l);

			this.setPageName(defaultPage);
		} catch (Exception e) {
			super.sendMessage(e);
		}
	}

	private void refresh() {
		try {
			readService.refreshEntityManager();
			funcionario = (Funcionario) readService.refresh(funcionario);
		} catch (Exception e) {
			super.sendMessage(e);
		}
	}

	private void close() {
		try {
			readService.refreshEntityManager();
			funcionario = instantiateService.instantiateFuncionario(0l);
		} catch (Exception e) {
			super.sendMessage(e);
		}
	}

	@Override
	public String actionLogout() {
		try {
			this.close();
			return getBeanName(IndexControllerImp.class);
		} catch (Exception e) {
			super.sendMessage(e);
		}
		return null;
	}

	@Override
	public String actionRefresh() {
		try {
			this.refresh();
			return getBeanName(this.getClass());
		} catch (Exception e) {
			super.sendMessage(e);
		}
		return null;
	}

	@Override
	public String redirectRegistro() {
		try {
			if (forcePasswordChange) {
				super.sendMessage(new WarnException(550014));
			} else {
				this.refresh();
				funcionarioRegistroController = super
						.findBean(FuncionarioRegistroController.class);
				funcionarioRegistroController.actionClear();
				this.setPageName(Constants.PN_FUNCIONARIO_REGISTRO);
				return getBeanName(this.getClass());
			}
		} catch (Exception e) {
			super.sendMessage(e);
		}
		return null;
	}

	@Override
	public String redirectCadastro() {
		try {
			this.refresh();
			funcionarioCadastroController = super
					.findBean(FuncionarioCadastroController.class);
			funcionarioCadastroController.actionClear();
			this.setPageName(Constants.PN_FUNCIONARIO_CADASTRO);
			return getBeanName(this.getClass());
		} catch (Exception e) {
			super.sendMessage(e);
		}
		return null;
	}

	@Override
	public Funcionario getFuncionario() {
		return this.funcionario;
	}

	@Override
	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}

	@Override
	public void setForcePasswordChange(boolean bool) {
		if (bool) {
			this.setPageName(Constants.PN_FUNCIONARIO_CADASTRO);
		} else {
			this.setPageName(defaultPage);
		}
		forcePasswordChange = bool;
	}

	@Override
	public void unblockPasswordChange() {
		forcePasswordChange = false;
	}

}