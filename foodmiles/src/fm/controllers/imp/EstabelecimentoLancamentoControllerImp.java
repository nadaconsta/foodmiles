package fm.controllers.imp;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import fm.controllers.EstabelecimentoController;
import fm.controllers.EstabelecimentoLancamentoController;
import fm.entity.Estabelecimento;
import fm.entity.RegistroPontuacao;
import fm.exceptions.InfoException;
import fm.services.GenerateService;
import fm.services.InstantiateService;
import fm.services.UpdateService;

@SessionScoped
@ManagedBean(eager = true)
public class EstabelecimentoLancamentoControllerImp extends
		GenericControllerImp implements EstabelecimentoLancamentoController,
		Serializable {
	private static final long serialVersionUID = -3028507600797341114L;

	RegistroPontuacao registroPontuacao;

	// Parent Controller
	EstabelecimentoController estabelecimentoController;

	UpdateService updateService;
	GenerateService generateService;
	InstantiateService instantiateService;

	@Override
	@PostConstruct
	public void init() {
		updateService = super.findBean(UpdateService.class);
		generateService = super.findBean(GenerateService.class);
		instantiateService = super.findBean(InstantiateService.class);

		estabelecimentoController = super
				.findBean(EstabelecimentoController.class);

		registroPontuacao = instantiateService.instantiateRegistroPontuacao();
	}

	private void refresh() {
		try {
			estabelecimentoController.actionRefresh();
		} catch (Exception e) {
			super.sendMessage(e);
		}
	}

	private void clear() {
		try {
			registroPontuacao = instantiateService
					.instantiateRegistroPontuacao();
		} catch (Exception e) {
			super.sendMessage(e);
		}
	}

	@Override
	public void actionClear() {
		try {
			this.clear();
		} catch (Exception e) {
			super.sendMessage(e);
		}
	}

	@Override
	public Estabelecimento getEstabelecimento() {
		return estabelecimentoController.getEstabelecimento();
	}

	@Override
	public String actionApprove() {
		try {
			registroPontuacao.setFlgestornosolicitado(false);
			registroPontuacao.setFlgestornoreprovado(false);
			registroPontuacao.setFlgestornoaprovado(true);
			updateService.updateRegistroPontuacao(registroPontuacao);
			this.refresh();

			super.sendMessage(new InfoException(330006));

			return getBeanName(EstabelecimentoController.class);
		} catch (Exception e) {
			super.sendMessage(e);
		}
		return null;
	}

	@Override
	public String actionReject() {
		try {
			registroPontuacao.setFlgestornosolicitado(false);
			registroPontuacao.setFlgestornoaprovado(false);
			registroPontuacao.setFlgestornoreprovado(true);
			updateService.updateRegistroPontuacao(registroPontuacao);
			this.refresh();

			super.sendMessage(new InfoException(330007));

			return getBeanName(EstabelecimentoController.class);
		} catch (Exception e) {
			super.sendMessage(e);
		}
		return null;
	}

	@Override
	public List<RegistroPontuacao> getListRegistroPontuacaoRequestedByEstabelecimento() {
		try {
			return generateService
					.generateListRegistroPontuacaoRequestedByEstabelecimento(estabelecimentoController
							.getEstabelecimento());
		} catch (Exception e) {
			super.sendMessage(e);
		}
		return new ArrayList<RegistroPontuacao>();
	}

	@Override
	public List<RegistroPontuacao> getListRegistroPontuacaoApprovedByEstabelecimento() {
		try {
			return generateService
					.generateListRegistroPontuacaoApprovedByEstabelecimento(estabelecimentoController
							.getEstabelecimento());
		} catch (Exception e) {
			super.sendMessage(e);
		}
		return new ArrayList<RegistroPontuacao>();
	}

	@Override
	public List<RegistroPontuacao> getListRegistroPontuacaoRejectedByEstabelecimento() {
		try {
			return generateService
					.generateListRegistroPontuacaoRejectedByEstabelecimento(estabelecimentoController
							.getEstabelecimento());
		} catch (Exception e) {
			super.sendMessage(e);
		}
		return new ArrayList<RegistroPontuacao>();
	}

	@Override
	public RegistroPontuacao getRegistroPontuacao() {
		return registroPontuacao;
	}

	@Override
	public void setRegistroPontuacao(RegistroPontuacao registroPontuacao) {
		this.registroPontuacao = registroPontuacao;
	}

}
