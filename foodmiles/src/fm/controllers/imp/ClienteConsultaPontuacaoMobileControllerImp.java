package fm.controllers.imp;

import java.io.Serializable;
import java.text.ParseException;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.persistence.NoResultException;

import fm.constants.Constants;
import fm.controllers.ClienteConsultaPontuacaoMobileController;
import fm.controllers.ClienteVisualizaPontuacaoMobileController;
import fm.entity.Cliente;
import fm.entity.Estabelecimento;
import fm.exceptions.ErrorException;
import fm.exceptions.InfoException;
import fm.exceptions.WarnException;
import fm.services.CreateService;
import fm.services.FormatService;
import fm.services.InstantiateService;
import fm.services.ReadService;
import fm.services.ValidateService;

@SessionScoped
@ManagedBean(eager = true)
public class ClienteConsultaPontuacaoMobileControllerImp extends GenericControllerImp
		implements ClienteConsultaPontuacaoMobileController, Serializable {
	private static final long serialVersionUID = -2091276319584849879L;
	
	Cliente cliente;
	Estabelecimento estabelecimento;

	ReadService readService;
	CreateService createService;
	FormatService formatService;
	ValidateService validateService;
	InstantiateService instantiateService;
	
	// Sub Controllers
	ClienteVisualizaPontuacaoMobileController clienteVisualizaPontuacaoMobileController;

	@Override
	@PostConstruct
	public void init() {
		try {
			readService = super.findBean(ReadService.class);
			createService = super.findBean(CreateService.class);
			formatService = super.findBean(FormatService.class);
			validateService = super.findBean(ValidateService.class);
			instantiateService = super.findBean(InstantiateService.class);
			
			cliente = instantiateService.instantiateCliente(0l);
			estabelecimento = instantiateService.instantiateEstabelecimento(0l);
			
			this.setMobile(true);
		} catch (Exception e) {
			super.sendMessage(e);
		}
	}

	@Override
	public Estabelecimento getEstabelecimento() {
		return estabelecimento;
	}

	@Override
	public void setEstabelecimento(Estabelecimento estabelecimento) {
		this.estabelecimento = estabelecimento;
	}

	@Override
	public String actionSearch() {
		try {
			// Searches for the cliente by CPF
			boolean validated = true;
			if (!validateService.isValidCPF(cliente.getNumcpf())) {
				validated = false;
				super.sendMessage(new WarnException(550002));
			}
			if (validated) {
				Long cpf = cliente.getNumcpf();
				Cliente resultCliente = readService.readClienteByCPF(cpf);
				//Cliente found, validate the cliente registration
				cliente = resultCliente;
				if (validateService.isClienteCompletlyRegistered(cliente)) {
					clienteVisualizaPontuacaoMobileController = super.findBean(ClienteVisualizaPontuacaoMobileController.class);
					clienteVisualizaPontuacaoMobileController.setCliente(cliente);
					clienteVisualizaPontuacaoMobileController.setEstabelecimento(estabelecimento);
					return clienteVisualizaPontuacaoMobileController.getBeanName();
				} else {
					// send to email register
				}
				//TODO Acertar mensagens para essa tela
				super.sendMessage(new InfoException(330003));
			}
		} catch (NoResultException e) {
			//TODO Criar novo pr� cadastro e enviar para email register
			
			//TODO Acertar mensagens para essa tela
			super.sendMessage(new InfoException(330002));
		} catch (Exception e) {
			super.sendMessage(e);
		}
		return null;
	}
	
	@Override
	public String getClienteNumcpf() {
		try {
			Long numcpf = cliente.getNumcpf();
			if (numcpf.intValue() == Constants.ZERO_INT) {
				return null;
			}
			String cpf = formatService.fillLeft(numcpf.toString(),
					Constants.ZERO_STRING, Constants.CPF_SIZE);
			return formatService.formatString(cpf, Constants.MASK_CPF);
		} catch (ParseException e) {
			super.sendMessage(new ErrorException(770002));
		} catch (Exception e) {
			super.sendMessage(e);
		}
		return null;
	}

	@Override
	public void setClienteNumcpf(String strcpf) {
		try {
			cliente = instantiateService.instantiateCliente(0l);
			cliente.setNumcpf(formatService.formatCPFLong(strcpf));
		} catch (Exception e) {
			super.sendMessage(e);
		}
	}

}
