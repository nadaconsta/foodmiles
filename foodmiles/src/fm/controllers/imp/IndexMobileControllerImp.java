package fm.controllers.imp;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.persistence.NoResultException;

import org.apache.commons.lang3.NotImplementedException;

import fm.controllers.ClienteConsultaPontuacaoMobileController;
import fm.controllers.EstabelecimentoController;
import fm.controllers.FuncionarioController;
import fm.controllers.IndexController;
import fm.entity.Estabelecimento;
import fm.entity.Funcionario;
import fm.exceptions.WarnException;
import fm.services.FormatService;
import fm.services.InstantiateService;
import fm.services.ReadService;
import fm.services.ValidateService;

@ViewScoped
@ManagedBean(eager = true)
public class IndexMobileControllerImp extends GenericControllerImp implements
IndexController, Serializable {
	private static final long serialVersionUID = 4737061511956477976L;
	
	Funcionario funcionario;
	Estabelecimento estabelecimento;

	// Sub Controllers
	FuncionarioController funcionarioController;
	EstabelecimentoController estabelecimentoController;
	ClienteConsultaPontuacaoMobileController clienteConsultaPontuacaoMobileController;

	ReadService readService;
	FormatService formatService;
	ValidateService validateService;
	InstantiateService instantiateService;

	@Override
	@PostConstruct
	public void init() {
		try {
			readService = super.findBean(ReadService.class);
			formatService = super.findBean(FormatService.class);
			validateService = super.findBean(ValidateService.class);
			instantiateService = super.findBean(InstantiateService.class);

			funcionarioController = super.findBean(FuncionarioController.class);
			estabelecimentoController = super.findBean(EstabelecimentoController.class);
			clienteConsultaPontuacaoMobileController = super.findBean(ClienteConsultaPontuacaoMobileController.class);

			readService.refreshEntityManager();

			funcionario = instantiateService.instantiateFuncionario(0l);
			estabelecimento = instantiateService.instantiateEstabelecimento(0l);
			funcionario.setEstabelecimento(estabelecimento);
			
			this.setMobile(true);
		} catch (Exception e) {
			super.sendMessage(e);
		}
	}

	@Override
	public String actionLoginEstabelecimento() {
		try {
			boolean validated = true;
			if (!validateService.isValidCNPJ(estabelecimento.getNumcnpj())) {
				validated = false;
				super.sendMessage(new WarnException(550002));
			}
			if (validated) {
				// TODO criptografia de senha.
				estabelecimento = readService.readEstabelecimentoByCNPJSenha(
						estabelecimento.getNumcnpj(),
						estabelecimento.getStrsenha());

				clienteConsultaPontuacaoMobileController.setEstabelecimento(estabelecimento);
				return clienteConsultaPontuacaoMobileController.getBeanName();
			}
		} catch (NoResultException e) {
			super.sendMessage(new WarnException(550001));
		} catch (Exception e) {
			super.sendMessage(e);
		}
		return this.getBeanName();
	}

	@Override
	public String actionLoginFuncionario() {
		throw new NotImplementedException(this.getBeanName());
	}

	@Override
	public void setFuncionarioNumcpf(String strcpf) {
		throw new NotImplementedException(this.getBeanName());
	}

	@Override
	public String getFuncionarioNumcpf() {
		throw new NotImplementedException(this.getBeanName());
	}

	@Override
	public void setEstabelecimentoNumcnpj(String strcnpj) {
		try {
			this.estabelecimento.setNumcnpj(formatService
					.formatCNPJLong(strcnpj));
		} catch (Exception e) {
			super.sendMessage(e);
		}
	}

	@Override
	public String getEstabelecimentoNumcnpj() {
		try {
			return formatService.formatCNPJString(this.estabelecimento
					.getNumcnpj());
		} catch (Exception e) {
			super.sendMessage(e);
		}
		return null;
	}

	@Override
	public Estabelecimento getEstabelecimento() {
		return estabelecimento;
	}

	@Override
	public void setEstabelecimento(Estabelecimento estabelecimento) {
		this.estabelecimento = estabelecimento;
	}

	@Override
	public Funcionario getFuncionario() {
		throw new NotImplementedException(this.getBeanName());
	}

	@Override
	public void setFuncionario(Funcionario funcionario) {
		throw new NotImplementedException(this.getBeanName());
	}
}
