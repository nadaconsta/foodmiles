package fm.controllers;

import java.util.List;

import fm.entity.Estabelecimento;
import fm.entity.Funcionario;

public interface EstabelecimentoFuncionarioController extends GenericController {

	String actionSave();

	String actionCreate();

	String actionResetPassword();

	Estabelecimento getEstabelecimento();

	List<Funcionario> getListFuncionarioByEstabelecimento();

	Funcionario getFuncionario();

	String getFuncionarioNumcpf();

	void setFuncionarioNumcpf(String strcpf);

	void setFuncionario(Funcionario funcionario);

	String getfuncionarioTelefone();

	void setfuncionarioTelefone(String strTelefone);

	String getfuncionarioCelular();

	void setfuncionarioCelular(String strCelular);

	boolean isActionResetPasswordEnabled();

}
