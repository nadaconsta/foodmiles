package fm.controllers;

import fm.entity.Funcionario;

public interface FuncionarioCadastroController extends GenericController {

	String actionSave();

	Funcionario getFuncionario();

	String getFuncionarioNumcpf();

	void setFuncionarioNumcpf(String strcpf);

	String getfuncionarioTelefone();

	void setfuncionarioTelefone(String strTelefone);

	String getfuncionarioCelular();

	void setfuncionarioCelular(String strCelular);

}
