package fm.controllers;

import fm.entity.Estabelecimento;

public interface EstabelecimentoController extends GenericController {

	String actionLogout();

	String actionRefresh();

	String redirectLancamento();

	String redirectFuncionario();

	Estabelecimento getEstabelecimento();

	void setEstabelecimento(Estabelecimento estabelecimento);

}
