package fm.controllers;

import java.util.List;

import fm.entity.Estabelecimento;
import fm.entity.RegistroPontuacao;

public interface EstabelecimentoLancamentoController extends GenericController {

	String actionApprove();

	String actionReject();

	Estabelecimento getEstabelecimento();

	List<RegistroPontuacao> getListRegistroPontuacaoRequestedByEstabelecimento();

	List<RegistroPontuacao> getListRegistroPontuacaoApprovedByEstabelecimento();

	List<RegistroPontuacao> getListRegistroPontuacaoRejectedByEstabelecimento();

	RegistroPontuacao getRegistroPontuacao();

	void setRegistroPontuacao(RegistroPontuacao registroPontuacao);

}
