package fm.controllers;

import fm.entity.Funcionario;

public interface FuncionarioController extends GenericController {

	String actionLogout();

	String actionRefresh();

	String redirectRegistro();

	String redirectCadastro();

	Funcionario getFuncionario();

	void setFuncionario(Funcionario funcionario);

	void setForcePasswordChange(boolean bool);
	
	void unblockPasswordChange();

}
