package fm.controllers;

import fm.entity.Estabelecimento;

public interface ClienteConsultaPontuacaoMobileController extends GenericController {

	String actionSearch();

	Estabelecimento getEstabelecimento();

	void setEstabelecimento(Estabelecimento estabelecimento);

	String getClienteNumcpf();

	void setClienteNumcpf(String strcpf);

}
