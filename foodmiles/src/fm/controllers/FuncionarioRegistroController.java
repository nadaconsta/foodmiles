package fm.controllers;

import java.util.Date;
import java.util.List;

import fm.entity.Cliente;
import fm.entity.Funcionario;
import fm.entity.RegistroPontuacao;

public interface FuncionarioRegistroController extends GenericController {

	String actionSearch();

	String actionRegister();

	String actionSubtract();

	String actionReverse();

	List<String> listClienteCPFByPartialCPF(String value);

	List<String> listClienteNomeSobrenomeByPartialNomeSobrenome(String value);

	List<Cliente> getListClienteByFrequency();

	List<RegistroPontuacao> getListRegistroPontuacaoRecenteByFuncionario();

	Funcionario getFuncionario();

	Cliente getCliente();

	void setCliente(Cliente cliente);

	String getClienteNumcpf();

	void setClienteNumcpf(String numcpf);

	String getClienteNomeSobrenome();

	void setClienteNomeSobrenome(String nomeSobrenome);

	String getClienteEmpresa();

	void setClienteEmpresa(String nomeEmpresa);

	boolean isActionRegisterEnabled();

	boolean isActionSubtractEnabled();

	boolean isActionCreditEnabled();

	boolean isNoClientSearch();

	RegistroPontuacao getRegistroPontuacao();

	void setRegistroPontuacao(RegistroPontuacao registroPontuacao);

	int getNumRetirada();

	void setNumRetirada(int numRetirada);

	void setNumCredito(int numCredito);

	int getNumCredito();

	Integer getSaldoAposRetirada();

	Integer getSaldoAposCredito();

	Integer getVisitaRecente();

	Integer getVisitaTotal();

	Integer getSaldoPontos();

	String actionCredit();

	Date getVisitaUltima();

}
