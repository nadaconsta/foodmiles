package fm.constants;

public final class Constants {
	// Strings
	public static final String EMPTY_STRING = "";

	// Masks
	public static final String MASK_CPF = "###.###.###-##";
	public static final String MASK_CNPJ = "##.###.###/####-##";
	public static final String MASK_TELEFONE = "(##) ####-####";
	public static final String MASK_CELULAR = "(##) #####-####";
	public static final String ZERO_CPF = "00000000000";
	public static final String ZERO_CNPJ = "00000000000000";

	// Reg Exp
	public static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
			+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	public static final String TELEFONE_PATTERN = "[1-9][1-9][2-5][0-9]{7}";
	public static final String CELULAR_8_PATTERN = "[1-9][1-9][6-9][0-9]{7}";
	public static final String CELULAR_9_PATTERN = "[1-9][1-9][9][5-9][0-9]{7}";
	public static final String PASSWORD_PATTERN = "^(?=.*[\\d])(?=.*[A-Z])(?=.*[a-z])[\\w\\d!@#$%_]{6,40}$";

	// Integers
	public static final int DDD_CELULAR_LENGHT = 11;
	public static final int DDD_TELEFONE_LENGHT = 10;
	public static final int ZERO_INT = 0;
	public static final int ONE_INT = 1;
	public static final String ZERO_STRING = "0";
	public static final int CPF_SIZE = 11;
	public static final int CNPJ_SIZE = 14;
	public static final Integer FATAL_CODE = 999999;

	// Standard page actions
	public static final String PG_BACK = "back";
	public static final String DEFAULT_SEPARATOR = "_";

	// Page names
	public static final String PN_FUNCIONARIO_REGISTRO = "/pages/funcionario.registro.xhtml";
	public static final String PN_FUNCIONARIO_CADASTRO = "/pages/funcionario.cadastro.xhtml";
	public static final String PN_ESTABELECIMENTO_LANCAMENTO = "/pages/estabelecimento.lancamento.xhtml";
	public static final String PN_ESTABELECIMENTO_FUNCIONARIO = "/pages/estabelecimento.funcionario.xhtml";

	// Msgs propety name
	public static final String APPROVED = "registro_pontuacao_flgestornoaprovado";
	public static final String REJECTED = "registro_pontuacao_flgestornoreprovado";
	public static final String REQUESTED = "registro_pontuacao_flgestornosolicitado";
	public static final String YES = "yes";
	public static final String NO = "no";
	// Other Strings
	public static final String SPACE = " ";
	public static final String OPEN_BRACKET = "(";
	public static final String CLOSE_BRACKET = ")";
	public static final String LIKEPERCENTAGE = "%";
	public static final String UM_STRING = "1";
	public static final int MESES_RECENTES = -1;

}
