package fm.dao;

import javax.persistence.EntityManager;

import fm.managed.bean.GenericManagedBean;

public interface EntityManagerFactoryDao extends GenericManagedBean {
	void init();

	EntityManager createEntityManager();
}
