package fm.dao;

import fm.entity.Pontuacao;

public interface PontuacaoDao extends GenericDao {
	Pontuacao createPontuacao(Pontuacao pontuacao);

	Pontuacao updatePontuacao(Pontuacao pontuacao);

	Pontuacao findValidPontuacaoByIdEstabelecimento(int idestabelecimento);

}
