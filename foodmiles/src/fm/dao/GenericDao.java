package fm.dao;

import javax.persistence.TypedQuery;

import fm.entity.gen.GenericEntity;
import fm.managed.bean.GenericManagedBean;

public interface GenericDao extends GenericManagedBean {
	<T> GenericEntity<T> findById(Integer id, Class<T> class_);

	void refreshEntityManager();

	Boolean delete(GenericEntity<?> entity);

	<T> TypedQuery<T> createQuery(String query, Class<T> class_);

	<T> GenericEntity<T> saveOrUpdate(GenericEntity<T> entity);

	<T> GenericEntity<T> update(GenericEntity<T> entity);

	<T> GenericEntity<T> refresh(GenericEntity<T> entity);

}
