package fm.dao;

import fm.managed.bean.GenericManagedBean;

public interface EntityManagerDao extends GenericManagedBean, GenericDao {
	void init();

	void rollback();

	void refreshEntityManager();
}
