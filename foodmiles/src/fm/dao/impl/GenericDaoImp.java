package fm.dao.impl;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import fm.constants.Constants;
import fm.dao.EntityManagerDao;
import fm.dao.GenericDao;
import fm.entity.gen.GenericEntity;
import fm.managed.bean.imp.GenericManagedBeanImp;

@ApplicationScoped
@ManagedBean(eager = true)
public class GenericDaoImp extends GenericManagedBeanImp implements GenericDao {
	private EntityManagerDao entityManagerDao;

	@Override
	@Transactional
	public <T> GenericEntity<T> saveOrUpdate(GenericEntity<T> entity) {
		try {
			entityManagerDao = findBean(EntityManagerDao.class);
			return entityManagerDao.saveOrUpdate(entity);
		} catch (RuntimeException e) {
			entityManagerDao.rollback();
			throw e;
		}
	}

	@Override
	@Transactional
	public <T> GenericEntity<T> update(GenericEntity<T> entity) {
		try {
			entityManagerDao = findBean(EntityManagerDao.class);
			return entityManagerDao.update(entity);
		} catch (RuntimeException e) {
			entityManagerDao.rollback();
			throw e;
		}
	}

	@Override
	@Transactional
	public Boolean delete(GenericEntity<?> entity) {
		try {
			entityManagerDao = findBean(EntityManagerDao.class);
			return entityManagerDao.delete(entity);
		} catch (RuntimeException e) {
			entityManagerDao.rollback();
			throw e;
		}
	}

	@Override
	@Transactional
	public <T> TypedQuery<T> createQuery(String query, Class<T> class_) {
		try {
			entityManagerDao = findBean(EntityManagerDao.class);
			return entityManagerDao.createQuery(query, class_);
		} catch (RuntimeException e) {
			entityManagerDao.rollback();
			throw e;
		}
	}

	@Override
	@Transactional
	public <T> GenericEntity<T> findById(Integer id, Class<T> class_) {
		try {
			entityManagerDao = findBean(EntityManagerDao.class);
			return entityManagerDao.findById(id, class_);
		} catch (RuntimeException e) {
			entityManagerDao.rollback();
			throw e;
		}
	}

	@Override
	@Transactional
	public <T> GenericEntity<T> refresh(GenericEntity<T> entity) {
		try {
			entityManagerDao = findBean(EntityManagerDao.class);
			if (entity.getId() != Constants.ZERO_INT) {
				return entityManagerDao.refresh(entity);
			} else {
				return entity;
			}
		} catch (RuntimeException e) {
			entityManagerDao.rollback();
			throw e;
		}
	}

	@Override
	public void refreshEntityManager() {
		entityManagerDao = findBean(EntityManagerDao.class);
		entityManagerDao.refreshEntityManager();
	}

}
