package fm.dao.impl;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

import fm.dao.RegistroPontuacaoDao;
import fm.entity.RegistroPontuacao;

@ApplicationScoped
@ManagedBean(eager = true)
public class RegistroPontuacaoDaoImp extends GenericDaoImp implements RegistroPontuacaoDao {

	@Override
	public RegistroPontuacao createRegistroPontuacao(RegistroPontuacao pontuacao) {
		return (RegistroPontuacao) saveOrUpdate(pontuacao);
	}

	@Override
	public RegistroPontuacao updateRegistroPontuacao(RegistroPontuacao pontuacao) {
		return (RegistroPontuacao) update(pontuacao);
	}

}
