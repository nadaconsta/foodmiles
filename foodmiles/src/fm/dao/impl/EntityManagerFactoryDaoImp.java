package fm.dao.impl;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import fm.constants.ConfigConstants;
import fm.dao.EntityManagerFactoryDao;
import fm.managed.bean.imp.GenericManagedBeanImp;

@ApplicationScoped
@ManagedBean
public class EntityManagerFactoryDaoImp extends GenericManagedBeanImp implements
		EntityManagerFactoryDao, Serializable {
	private static final long serialVersionUID = -432324243149943980L;
	private EntityManagerFactory emf;

	@Override
	@PostConstruct
	public void init() {
		emf = Persistence
				.createEntityManagerFactory(ConfigConstants.PERSISTANCE_UNIT);
	}

	@Override
	public EntityManager createEntityManager() {
		return emf.createEntityManager();
	}
}
