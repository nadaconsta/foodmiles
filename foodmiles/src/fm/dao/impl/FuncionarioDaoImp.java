package fm.dao.impl;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.persistence.TypedQuery;

import fm.dao.FuncionarioDao;
import fm.entity.Funcionario;

@ApplicationScoped
@ManagedBean(eager = true)
public class FuncionarioDaoImp extends GenericDaoImp implements FuncionarioDao {

	@Override
	
	public Funcionario createFuncionario(Funcionario funcionario) {
		return (Funcionario) saveOrUpdate(funcionario);
	}

	@Override
	public Funcionario updateFuncionario(Funcionario funcionario) {
		return (Funcionario) saveOrUpdate(funcionario);
	}

	@Override
	public Funcionario findFuncionarioByCPFSenha(Long cpf, String senha) {
		TypedQuery<Funcionario> query = createQuery("Funcionario.findByCPFSenha",
				Funcionario.class);
		query.setParameter("cpf", cpf);
		query.setParameter("senha", senha);
		return query.getSingleResult();
	}
	
	@Override
	public Funcionario findFuncionarioByCPFSenhaNull(Long cpf) {
		TypedQuery<Funcionario> query = createQuery("Funcionario.findByCPFSenhaNull",
				Funcionario.class);
		query.setParameter("cpf", cpf);
		return query.getSingleResult();
	}

	
}
