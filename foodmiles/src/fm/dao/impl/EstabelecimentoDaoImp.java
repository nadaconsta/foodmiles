package fm.dao.impl;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import fm.dao.EstabelecimentoDao;
import fm.entity.Estabelecimento;

@ApplicationScoped
@Transactional
@ManagedBean(eager = true)
public class EstabelecimentoDaoImp extends GenericDaoImp implements EstabelecimentoDao {

	@Override
	public Estabelecimento createEstabelecimento(Estabelecimento Estabelecimento) {
		return (Estabelecimento) saveOrUpdate(Estabelecimento);
	}

	@Override
	public Estabelecimento updateEstabelecimento(Estabelecimento Estabelecimento) {
		return (Estabelecimento) saveOrUpdate(Estabelecimento);
	}

	@Override
	public Estabelecimento findEstabelecimentoByCNPJSenha(Long cnpj, String senha) {
		TypedQuery<Estabelecimento> query = createQuery("Estabelecimento.findByCNPJSenha", Estabelecimento.class);
		query.setParameter("cnpj", cnpj);
		query.setParameter("senha", senha);
		return query.getSingleResult();
	}

}
