package fm.dao.impl;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.persistence.TypedQuery;

import fm.constants.Constants;
import fm.dao.PontuacaoDao;
import fm.entity.Pontuacao;

@ApplicationScoped
@ManagedBean(eager = true)
public class PontuacaoDaoImp extends GenericDaoImp implements PontuacaoDao {

	@Override
	public Pontuacao createPontuacao(Pontuacao pontuacao) {
		return (Pontuacao) saveOrUpdate(pontuacao);
	}

	@Override
	public Pontuacao updatePontuacao(Pontuacao pontuacao) {
		return (Pontuacao) saveOrUpdate(pontuacao);
	}

	@Override
	public Pontuacao findValidPontuacaoByIdEstabelecimento(int idestabelecimento) {
		TypedQuery<Pontuacao> query = createQuery("Pontuacao.findByIdEstabelecimentoOrderDescDttInclusao",
				Pontuacao.class);
		query.setParameter("idestabelecimento", idestabelecimento);
		query.setMaxResults(Constants.ONE_INT);

		return query.getSingleResult();
	}

}
