package fm.dao.impl;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import fm.dao.EmpresaDao;
import fm.entity.Empresa;

@ApplicationScoped
@ManagedBean(eager = true)
public class EmpresaDaoImp extends GenericDaoImp implements EmpresaDao {

	@Override
	public Empresa createEmpresa(Empresa empresa) {
		return (Empresa) saveOrUpdate(empresa);
	}

	@Override
	public Empresa updateEmpresa(Empresa empresa) {
		return (Empresa) saveOrUpdate(empresa);
	}

	@Override
	public Empresa findEmpresaByNome(String nome) throws NoResultException {
		TypedQuery<Empresa> query = createQuery("Empresa.findByNome", Empresa.class);
		query.setParameter("nome", nome);
		return query.getSingleResult();
	}

}
