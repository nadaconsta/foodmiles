package fm.dao.impl;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import fm.dao.EntityManagerDao;
import fm.dao.EntityManagerFactoryDao;
import fm.entity.gen.GenericEntity;
import fm.exceptions.ErrorException;
import fm.managed.bean.imp.GenericManagedBeanImp;

@SessionScoped
@ManagedBean
public class EntityManagerDaoImp extends GenericManagedBeanImp implements
		EntityManagerDao, Serializable {
	private static final long serialVersionUID = -432324243149943980L;
	private EntityManager em;

	EntityManagerFactoryDao entityManagerFactoryDao;

	@Override
	@PostConstruct
	public void init() {
		entityManagerFactoryDao = super.findBean(EntityManagerFactoryDao.class);
		em = entityManagerFactoryDao.createEntityManager();
	}

	@Override
	public void refreshEntityManager() {
		em.clear();
		em.close();
		em = entityManagerFactoryDao.createEntityManager();
		if (!em.getTransaction().isActive()) {
			em.getTransaction().begin();
		}
	}

	@Override
	@Transactional
	public <T> GenericEntity<T> saveOrUpdate(GenericEntity<T> entity) {
		if (!em.getTransaction().isActive()) {
			em.getTransaction().begin();
		}

		if (em.contains(entity)) {
			em.persist(entity);
		} else {
			entity = em.merge(entity);
		}
		em.flush();
		em.getTransaction().commit();
		em.refresh(entity);
		return entity;
	}

	@Override
	public <T> GenericEntity<T> update(GenericEntity<T> entity) {
		if (!em.getTransaction().isActive()) {
			em.getTransaction().begin();
		}

		if (!em.contains(entity)) {
			throw new ErrorException("770003");
		}

		em.persist(entity);
		em.flush();
		em.getTransaction().commit();
		em.refresh(entity);
		return entity;
	}

	@Override
	@Transactional
	public Boolean delete(GenericEntity<?> entity) {
		if (!em.getTransaction().isActive()) {
			em.getTransaction().begin();
		}

		if (!em.contains(entity)) {
			entity = findById(entity.getId(), entity.getClass());
		}
		em.remove(entity);

		em.flush();
		em.getTransaction().commit();
		return true;
	}

	@Override
	@Transactional
	public <T> TypedQuery<T> createQuery(String query, Class<T> class_) {
		if (!em.getTransaction().isActive()) {
			em.getTransaction().begin();
		}
		return em.createNamedQuery(query, class_);
	}

	@Override
	@Transactional
	public void rollback() {
		em.getTransaction().rollback();
	}

	@Override
	@Transactional
	@SuppressWarnings("unchecked")
	public <T> GenericEntity<T> findById(Integer id, Class<T> class_) {
		GenericEntity<T> entity = (GenericEntity<T>) em.find(class_, id);
		em.refresh(entity);
		return entity;
	}

	@Override
	@Transactional
	@SuppressWarnings("unchecked")
	public <T> GenericEntity<T> refresh(GenericEntity<T> entity) {
		entity = (GenericEntity<T>) em.find(entity.getClass(), entity.getId());
		em.refresh(entity);
		return entity;
	}

}
