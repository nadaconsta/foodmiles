package fm.dao.impl;

import java.util.List;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.persistence.TypedQuery;

import fm.constants.Constants;
import fm.dao.ClienteDao;
import fm.entity.Cliente;

@ApplicationScoped
@ManagedBean(eager = true)
public class ClienteDaoImp extends GenericDaoImp implements ClienteDao {

	@Override
	public Cliente createCliente(Cliente cliente) {
		return (Cliente) saveOrUpdate(cliente);
	}

	@Override
	public Cliente updateCliente(Cliente cliente) {
		return (Cliente) update(cliente);
	}

	@Override
	public Cliente findClienteByCPF(Long cpf) {
		TypedQuery<Cliente> query = createQuery("Cliente.findByCPF",
				Cliente.class);
		query.setParameter("cpf", cpf);
		return query.getSingleResult();
	}

	@Override
	public List<Cliente> findClienteByRangeCPF(Long begin, Long end) {
		TypedQuery<Cliente> query = createQuery("Cliente.findByRangeCPF",
				Cliente.class);
		query.setParameter("begin", begin);
		query.setParameter("end", end);

		return query.getResultList();
	}

	@Override
	public List<Cliente> findClienteByNomeSobrenome(String value) {
		TypedQuery<Cliente> query = createQuery("Cliente.findByNomeSobrenome",
				Cliente.class);
		query.setParameter("space", Constants.SPACE);
		query.setParameter("nomesobrenome", Constants.LIKEPERCENTAGE + value
				+ Constants.LIKEPERCENTAGE);

		return query.getResultList();
	}

}
