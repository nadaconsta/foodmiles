package fm.dao;

import java.util.List;

import javax.persistence.NoResultException;

import fm.entity.Cliente;

public interface ClienteDao extends GenericDao {
	Cliente createCliente(Cliente cliente);

	Cliente updateCliente(Cliente cliente);

	Cliente findClienteByCPF(Long cpf) throws NoResultException;

	List<Cliente> findClienteByRangeCPF(Long begin, Long end);

	List<Cliente> findClienteByNomeSobrenome(String value);

}
