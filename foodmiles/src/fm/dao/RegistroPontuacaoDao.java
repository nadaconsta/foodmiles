package fm.dao;

import fm.entity.RegistroPontuacao;

public interface RegistroPontuacaoDao extends GenericDao {

	RegistroPontuacao createRegistroPontuacao(RegistroPontuacao registroPontuacao);

	RegistroPontuacao updateRegistroPontuacao(RegistroPontuacao registroPontuacao);
}
