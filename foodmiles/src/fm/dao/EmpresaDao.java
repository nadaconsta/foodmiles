package fm.dao;

import javax.persistence.NoResultException;

import fm.entity.Empresa;

public interface EmpresaDao extends GenericDao {
	Empresa createEmpresa(Empresa empresa);

	Empresa updateEmpresa(Empresa empresa);

	Empresa findEmpresaByNome(String nome) throws NoResultException;

}
