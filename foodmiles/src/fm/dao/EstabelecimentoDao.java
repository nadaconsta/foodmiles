package fm.dao;

import javax.persistence.NoResultException;

import fm.entity.Estabelecimento;

public interface EstabelecimentoDao extends GenericDao {
	Estabelecimento createEstabelecimento(Estabelecimento estabelecimento);

	Estabelecimento updateEstabelecimento(Estabelecimento estabelecimento);

	Estabelecimento findEstabelecimentoByCNPJSenha(Long cnpj, String senha) throws NoResultException;

}
