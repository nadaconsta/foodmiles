package fm.dao;

import javax.persistence.NoResultException;

import fm.entity.Funcionario;

public interface FuncionarioDao extends GenericDao {
	Funcionario createFuncionario(Funcionario funcionario);

	Funcionario updateFuncionario(Funcionario funcionario);

	Funcionario findFuncionarioByCPFSenha(Long cpf, String senha)
			throws NoResultException;

	Funcionario findFuncionarioByCPFSenhaNull(Long cpf)
			throws NoResultException;
}
