package fm.exceptions;

import java.util.ResourceBundle;

import javax.faces.context.FacesContext;

import fm.constants.ConfigConstants;

public class WarnException extends RuntimeException {
	private static final long serialVersionUID = 4520798195617186053L;

	private String message;
	private Integer code;

	public WarnException(Integer code) {
		this.code = code;
		ResourceBundle msgsBundle = FacesContext.getCurrentInstance().getApplication()
				.getResourceBundle(FacesContext.getCurrentInstance(), ConfigConstants.MSGS_PROPERTIES);
		this.message = msgsBundle.getString(code.toString());
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}
}
