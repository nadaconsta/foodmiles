package fm.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * The persistent class for the TBL_FUNCIONARIO database table.
 * 
 */
@Entity
@Table(name = "TBL_FUNCIONARIO")
@NamedQuery(name = "Funcionario.findAll", query = "SELECT f FROM Funcionario f")
public class Funcionario extends fm.entity.gen.GenericEntity<Funcionario>
		implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(unique = true, nullable = false)
	private int idfuncionario;

	@Temporal(TemporalType.TIMESTAMP)
	private Date dttinclusao;

	@Column(nullable = false)
	private long numcpf;

	private int numcelular;

	private int numdddcelular;
	// TODO Avaliar campos opcionais
	private int numtelefone;

	private int numdddtelefone;

	@Column(length = 255)
	private String stremail;

	@Column(length = 255)
	private String strnome;

	@Column(length = 255)
	private String strsenha;

	@Column(length = 255)
	private String strsobrenome;

	private boolean flgativo;

	// bi-directional many-to-one association to Estabelecimento
	@ManyToOne
	@JoinColumn(name = "IDESTABELECIMENTO", nullable = false)
	private Estabelecimento estabelecimento;

	// bi-directional many-to-one association to RegistroPontuacao
	@OneToMany(mappedBy = "funcionario")
	private List<RegistroPontuacao> lstRegistroPontuacao;

	public Funcionario() {
	}

	public int getIdfuncionario() {
		return this.idfuncionario;
	}

	public void setIdfuncionario(int idfuncionario) {
		this.idfuncionario = idfuncionario;
	}

	public Date getDttinclusao() {
		return this.dttinclusao;
	}

	public void setDttinclusao(Date dttinclusao) {
		this.dttinclusao = dttinclusao;
	}

	public int getNumtelefone() {
		return numtelefone;
	}

	public void setNumtelefone(int numtelefone) {
		this.numtelefone = numtelefone;
	}

	public int getNumdddtelefone() {
		return numdddtelefone;
	}

	public void setNumdddtelefone(int numdddtelefone) {
		this.numdddtelefone = numdddtelefone;
	}

	public int getNumcelular() {
		return this.numcelular;
	}

	public void setNumcelular(int numcelular) {
		this.numcelular = numcelular;
	}

	public int getNumdddcelular() {
		return numdddcelular;
	}

	public void setNumdddcelular(int numdddcelular) {
		this.numdddcelular = numdddcelular;
	}

	public long getNumcpf() {
		return this.numcpf;
	}

	public void setNumcpf(long numcpf) {
		this.numcpf = numcpf;
	}

	public String getStremail() {
		return this.stremail;
	}

	public void setStremail(String stremail) {
		this.stremail = stremail;
	}

	public String getStrnome() {
		return this.strnome;
	}

	public void setStrnome(String strnome) {
		this.strnome = strnome;
	}

	public String getStrsenha() {
		return this.strsenha;
	}

	public void setStrsenha(String strsenha) {
		this.strsenha = strsenha;
	}

	public String getStrsobrenome() {
		return this.strsobrenome;
	}

	public void setStrsobrenome(String strsobrenome) {
		this.strsobrenome = strsobrenome;
	}

	public boolean isFlgativo() {
		return flgativo;
	}

	public void setFlgativo(boolean flgativo) {
		this.flgativo = flgativo;
	}

	public Estabelecimento getEstabelecimento() {
		return this.estabelecimento;
	}

	public void setEstabelecimento(Estabelecimento estabelecimento) {
		this.estabelecimento = estabelecimento;
	}

	public List<RegistroPontuacao> getLstRegistroPontuacao() {
		return this.lstRegistroPontuacao;
	}

	public void setLstRegistroPontuacao(
			List<RegistroPontuacao> lstRegistroPontuacao) {
		this.lstRegistroPontuacao = lstRegistroPontuacao;
	}

	public RegistroPontuacao addLstRegistroPontuacao(
			RegistroPontuacao lstRegistroPontuacao) {
		getLstRegistroPontuacao().add(lstRegistroPontuacao);
		lstRegistroPontuacao.setFuncionario(this);

		return lstRegistroPontuacao;
	}

	public RegistroPontuacao removeLstRegistroPontuacao(
			RegistroPontuacao lstRegistroPontuacao) {
		getLstRegistroPontuacao().remove(lstRegistroPontuacao);
		lstRegistroPontuacao.setFuncionario(null);

		return lstRegistroPontuacao;
	}

}