package fm.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * The persistent class for the TBL_REGISTROPONTUACAO database table.
 * 
 */
@Entity
@Table(name = "TBL_REGISTROPONTUACAO")
@NamedQuery(name = "RegistroPontuacao.findAll", query = "SELECT r FROM RegistroPontuacao r")
public class RegistroPontuacao extends
		fm.entity.gen.GenericEntity<RegistroPontuacao> implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(unique = true, nullable = false)
	private int idregistropontuacao;

	@Temporal(TemporalType.TIMESTAMP)
	private Date dttinclusao;

	private Date dttreferencia;

	private int numpontos;

	private double valconta;

	private boolean flgnfpaulista;

	private boolean flggerarpontos;

	private boolean flgestornosolicitado;

	private boolean flgestornoaprovado;
	
	private boolean flgestornoreprovado;

	// bi-directional many-to-one association to Cliente
	@ManyToOne
	@JoinColumn(name = "IDCLIENTE", nullable = false)
	private Cliente cliente;

	// bi-directional many-to-one association to Estabelecimento
	@ManyToOne
	@JoinColumn(name = "IDESTABELECIMENTO", nullable = false)
	private Estabelecimento estabelecimento;

	// bi-directional many-to-one association to Funcionario
	@ManyToOne
	@JoinColumn(name = "IDFUNCIONARIO", nullable = false)
	private Funcionario funcionario;

	public RegistroPontuacao() {
	}

	public int getIdregistropontuacao() {
		return this.idregistropontuacao;
	}

	public void setIdregistropontuacao(int idregistropontuacao) {
		this.idregistropontuacao = idregistropontuacao;
	}

	public Date getDttinclusao() {
		return this.dttinclusao;
	}

	public void setDttinclusao(Date dttinclusao) {
		this.dttinclusao = dttinclusao;
	}

	public Date getDttreferencia() {
		return dttreferencia;
	}

	public void setDttreferencia(Date dttreferencia) {
		this.dttreferencia = dttreferencia;
	}

	public int getNumpontos() {
		return this.numpontos;
	}

	public void setNumpontos(int numpontos) {
		this.numpontos = numpontos;
	}

	public Cliente getCliente() {
		return this.cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Estabelecimento getEstabelecimento() {
		return this.estabelecimento;
	}

	public void setEstabelecimento(Estabelecimento estabelecimento) {
		this.estabelecimento = estabelecimento;
	}

	public Funcionario getFuncionario() {
		return this.funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}

	public boolean isFlgnfpaulista() {
		return flgnfpaulista;
	}

	public void setFlgnfpaulista(boolean flgnfpaulista) {
		this.flgnfpaulista = flgnfpaulista;
	}

	public boolean isFlggerarpontos() {
		return flggerarpontos;
	}

	public void setFlggerarpontos(boolean flggerarpontos) {
		this.flggerarpontos = flggerarpontos;
	}

	public boolean isFlgestornosolicitado() {
		return flgestornosolicitado;
	}

	public void setFlgestornosolicitado(boolean flgestornosolicitado) {
		this.flgestornosolicitado = flgestornosolicitado;
	}

	public boolean isFlgestornoaprovado() {
		return flgestornoaprovado;
	}

	public void setFlgestornoaprovado(boolean flgestornoaprovado) {
		this.flgestornoaprovado = flgestornoaprovado;
	}

	public boolean isFlgestornoreprovado() {
		return flgestornoreprovado;
	}

	public void setFlgestornoreprovado(boolean flgestornoreprovado) {
		this.flgestornoreprovado = flgestornoreprovado;
	}

	public double getValconta() {
		return valconta;
	}

	public void setValconta(double valconta) {
		this.valconta = valconta;
	}

}