package fm.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the TBL_EMPRESA database table.
 * 
 */
@Entity
@Table(name="TBL_EMPRESA")
@NamedQuery(name="Empresa.findAll", query="SELECT e FROM Empresa e")
public class Empresa extends fm.entity.gen.GenericEntity<Empresa> implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique=true, nullable=false)
	private int idempresa;

	@Temporal(TemporalType.TIMESTAMP)
	private Date dttinclusao;

	@Column(length=255)
	private String strnome;

	//bi-directional many-to-many association to Cliente
	@ManyToMany(mappedBy="lstEmpresa", cascade={CascadeType.ALL})
	private List<Cliente> lstCliente;

	//bi-directional many-to-many association to Estabelecimento
	@ManyToMany(mappedBy="lstEmpresa")
	private List<Estabelecimento> lstEstabelecimento;

	public Empresa() {
	}

	public int getIdempresa() {
		return this.idempresa;
	}

	public void setIdempresa(int idempresa) {
		this.idempresa = idempresa;
	}

	public Date getDttinclusao() {
		return this.dttinclusao;
	}

	public void setDttinclusao(Date dttinclusao) {
		this.dttinclusao = dttinclusao;
	}

	public String getStrnome() {
		return this.strnome;
	}

	public void setStrnome(String strnome) {
		this.strnome = strnome;
	}

	public List<Cliente> getLstCliente() {
		return this.lstCliente;
	}

	public void setLstCliente(List<Cliente> lstCliente) {
		this.lstCliente = lstCliente;
	}

	public List<Estabelecimento> getLstEstabelecimento() {
		return this.lstEstabelecimento;
	}

	public void setLstEstabelecimento(List<Estabelecimento> lstEstabelecimento) {
		this.lstEstabelecimento = lstEstabelecimento;
	}
}