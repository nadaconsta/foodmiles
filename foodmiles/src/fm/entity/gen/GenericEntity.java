package fm.entity.gen;

import java.lang.reflect.Method;

public class GenericEntity<T> {
	private Boolean selected;
	private String description;
	private Integer number;
	private Double value;

	public Boolean getSelected() {
		return selected;
	}

	public void setSelected(Boolean selected) {
		this.selected = selected;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getNumber() {
		return number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

	public Double getValue() {
		return value;
	}

	public void setValue(Double value) {
		this.value = value;
	}

	public Integer getId() {
		try {
			String idMethodName = "getId" + this.getClass().getSimpleName();
			Method[] methods = this.getClass().getDeclaredMethods();
			for (Method method : methods) {
				if (method.getName().equalsIgnoreCase(idMethodName)) {
					return (Integer) method.invoke(this);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public void setId(Integer id) {
		try {
			String idMethodName = "setId" + this.getClass().getSimpleName();
			Method[] methods = this.getClass().getDeclaredMethods();
			for (Method method : methods) {
				if (method.getName().equalsIgnoreCase(idMethodName)) {
					method.invoke(this, id);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean equals(Object entity) {
		if (this.getClass().equals(entity.getClass())
				&& this.getId().equals(((GenericEntity<T>) entity).getId())) {
			return true;
		} else {
			return false;
		}
	}

}
