package fm.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the TBL_PONTUACAO database table.
 * 
 */
@Entity
@Table(name="TBL_PONTUACAO")
@NamedQuery(name="Pontuacao.findAll", query="SELECT p FROM Pontuacao p")
public class Pontuacao extends fm.entity.gen.GenericEntity<Pontuacao> implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique=true, nullable=false)
	private int idpontuacao;

	@Temporal(TemporalType.TIMESTAMP)
	private Date dttinclusao;

	@Column(length=3)
	private String enmtipo;

	private int numvalor;

	//bi-directional many-to-one association to Estabelecimento
	@ManyToOne
	@JoinColumn(name="IDESTABELECIMENTO", nullable=false)
	private Estabelecimento estabelecimento;

	public Pontuacao() {
	}

	public int getIdpontuacao() {
		return this.idpontuacao;
	}

	public void setIdpontuacao(int idpontuacao) {
		this.idpontuacao = idpontuacao;
	}

	public Date getDttinclusao() {
		return this.dttinclusao;
	}

	public void setDttinclusao(Date dttinclusao) {
		this.dttinclusao = dttinclusao;
	}

	public String getEnmtipo() {
		return this.enmtipo;
	}

	public void setEnmtipo(String enmtipo) {
		this.enmtipo = enmtipo;
	}

	public int getNumvalor() {
		return this.numvalor;
	}

	public void setNumvalor(int numvalor) {
		this.numvalor = numvalor;
	}

	public Estabelecimento getEstabelecimento() {
		return this.estabelecimento;
	}

	public void setEstabelecimento(Estabelecimento estabelecimento) {
		this.estabelecimento = estabelecimento;
	}

}