package fm.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the TBL_MUNICIPIO database table.
 * 
 */
@Entity
@Table(name="TBL_MUNICIPIO")
@NamedQuery(name="Municipio.findAll", query="SELECT m FROM Municipio m")
public class Municipio extends fm.entity.gen.GenericEntity<Municipio> implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique=true, nullable=false)
	private int idmunicipio;

	private int numcodigoibge;

	@Column(length=255)
	private String strdescricao;

	//bi-directional many-to-one association to Estabelecimento
	@OneToMany(mappedBy="municipio", cascade={CascadeType.ALL})
	private List<Estabelecimento> lstEstabelecimento;

	//bi-directional many-to-one association to Estado
	@ManyToOne
	@JoinColumn(name="IDESTADO", nullable=false)
	private Estado estado;

	public Municipio() {
	}

	public int getIdmunicipio() {
		return this.idmunicipio;
	}

	public void setIdmunicipio(int idmunicipio) {
		this.idmunicipio = idmunicipio;
	}

	public int getNumcodigoibge() {
		return this.numcodigoibge;
	}

	public void setNumcodigoibge(int numcodigoibge) {
		this.numcodigoibge = numcodigoibge;
	}

	public String getStrdescricao() {
		return this.strdescricao;
	}

	public void setStrdescricao(String strdescricao) {
		this.strdescricao = strdescricao;
	}

	public List<Estabelecimento> getLstEstabelecimento() {
		return this.lstEstabelecimento;
	}

	public void setLstEstabelecimento(List<Estabelecimento> lstEstabelecimento) {
		this.lstEstabelecimento = lstEstabelecimento;
	}

	public Estabelecimento addLstEstabelecimento(Estabelecimento lstEstabelecimento) {
		getLstEstabelecimento().add(lstEstabelecimento);
		lstEstabelecimento.setMunicipio(this);

		return lstEstabelecimento;
	}

	public Estabelecimento removeLstEstabelecimento(Estabelecimento lstEstabelecimento) {
		getLstEstabelecimento().remove(lstEstabelecimento);
		lstEstabelecimento.setMunicipio(null);

		return lstEstabelecimento;
	}

	public Estado getEstado() {
		return this.estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}

}