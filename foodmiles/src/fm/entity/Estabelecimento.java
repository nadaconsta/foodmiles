package fm.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the TBL_ESTABELECIMENTO database table.
 * 
 */
@Entity
@Table(name="TBL_ESTABELECIMENTO")
@NamedQuery(name="Estabelecimento.findAll", query="SELECT e FROM Estabelecimento e")
public class Estabelecimento extends fm.entity.gen.GenericEntity<Estabelecimento> implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique=true, nullable=false)
	private int idestabelecimento;

	@Temporal(TemporalType.TIMESTAMP)
	private Date dttinclusao;

	private boolean flgbloqueado;

	private int numcep;

	@Column(nullable=false)
	private long numcnpj;

	private int numddd;

	private int numdddadc;

	private int numnumero;

	private int numtelefone;

	private int numtelefoneadc;

	@Column(length=255)
	private String strcomplemento;

	@Column(length=255)
	private String stremail;

	@Column(length=255)
	private String strlogradouro;

	@Column(length=255)
	private String strnomereduzido;

	@Column(length=255)
	private String strnomeresponsavel;

	@Column(length=255)
	private String strrazaosocial;

	@Column(length=255)
	private String strsenha;

	//bi-directional many-to-many association to Empresa
	@ManyToMany
	@JoinTable(
		name="TBL_ESTABELECIMENTOEMPRESA"
		, joinColumns={
			@JoinColumn(name="IDESTABELECIMENTO")
			}
		, inverseJoinColumns={
			@JoinColumn(name="IDEMPRESA")
			}
		)
	private List<Empresa> lstEmpresa;

	//bi-directional many-to-one association to Municipio
	@ManyToOne(cascade={CascadeType.ALL})
	@JoinColumn(name="IDMUNICIPIO", nullable=false)
	private Municipio municipio;

	//bi-directional many-to-one association to Funcionario
	@OneToMany(mappedBy="estabelecimento")
	private List<Funcionario> lstFuncionario;

	//bi-directional many-to-one association to Pontuacao
	@OneToMany(mappedBy="estabelecimento")
	private List<Pontuacao> lstPontuacao;

	//bi-directional many-to-one association to RegistroPontuacao
	@OneToMany(mappedBy="estabelecimento")
	private List<RegistroPontuacao> lstRegistroPontuacao;

	public Estabelecimento() {
	}

	public int getIdestabelecimento() {
		return this.idestabelecimento;
	}

	public void setIdestabelecimento(int idestabelecimento) {
		this.idestabelecimento = idestabelecimento;
	}

	public Date getDttinclusao() {
		return this.dttinclusao;
	}

	public void setDttinclusao(Date dttinclusao) {
		this.dttinclusao = dttinclusao;
	}

	public boolean isFlgbloqueado() {
		return flgbloqueado;
	}

	public void setFlgbloqueado(boolean flgbloqueado) {
		this.flgbloqueado = flgbloqueado;
	}
	
	public int getNumcep() {
		return this.numcep;
	}

	public void setNumcep(int numcep) {
		this.numcep = numcep;
	}

	public long getNumcnpj() {
		return this.numcnpj;
	}

	public void setNumcnpj(long numcnpj) {
		this.numcnpj = numcnpj;
	}

	public int getNumddd() {
		return this.numddd;
	}

	public void setNumddd(int numddd) {
		this.numddd = numddd;
	}

	public int getNumdddadc() {
		return this.numdddadc;
	}

	public void setNumdddadc(int numdddadc) {
		this.numdddadc = numdddadc;
	}

	public int getNumnumero() {
		return this.numnumero;
	}

	public void setNumnumero(int numnumero) {
		this.numnumero = numnumero;
	}

	public int getNumtelefone() {
		return this.numtelefone;
	}

	public void setNumtelefone(int numtelefone) {
		this.numtelefone = numtelefone;
	}

	public int getNumtelefoneadc() {
		return this.numtelefoneadc;
	}

	public void setNumtelefoneadc(int numtelefoneadc) {
		this.numtelefoneadc = numtelefoneadc;
	}

	public String getStrcomplemento() {
		return this.strcomplemento;
	}

	public void setStrcomplemento(String strcomplemento) {
		this.strcomplemento = strcomplemento;
	}

	public String getStremail() {
		return this.stremail;
	}

	public void setStremail(String stremail) {
		this.stremail = stremail;
	}

	public String getStrlogradouro() {
		return this.strlogradouro;
	}

	public void setStrlogradouro(String strlogradouro) {
		this.strlogradouro = strlogradouro;
	}

	public String getStrnomereduzido() {
		return this.strnomereduzido;
	}

	public void setStrnomereduzido(String strnomereduzido) {
		this.strnomereduzido = strnomereduzido;
	}

	public String getStrnomeresponsavel() {
		return this.strnomeresponsavel;
	}

	public void setStrnomeresponsavel(String strnomeresponsavel) {
		this.strnomeresponsavel = strnomeresponsavel;
	}

	public String getStrrazaosocial() {
		return this.strrazaosocial;
	}

	public void setStrrazaosocial(String strrazaosocial) {
		this.strrazaosocial = strrazaosocial;
	}

	public String getStrsenha() {
		return this.strsenha;
	}

	public void setStrsenha(String strsenha) {
		this.strsenha = strsenha;
	}

	public List<Empresa> getLstEmpresa() {
		return this.lstEmpresa;
	}

	public void setLstEmpresa(List<Empresa> lstEmpresa) {
		this.lstEmpresa = lstEmpresa;
	}

	public Municipio getMunicipio() {
		return this.municipio;
	}

	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}

	public List<Funcionario> getLstFuncionario() {
		return this.lstFuncionario;
	}

	public void setLstFuncionario(List<Funcionario> lstFuncionario) {
		this.lstFuncionario = lstFuncionario;
	}

	public Funcionario addLstFuncionario(Funcionario lstFuncionario) {
		getLstFuncionario().add(lstFuncionario);
		lstFuncionario.setEstabelecimento(this);

		return lstFuncionario;
	}

	public Funcionario removeLstFuncionario(Funcionario lstFuncionario) {
		getLstFuncionario().remove(lstFuncionario);
		lstFuncionario.setEstabelecimento(null);

		return lstFuncionario;
	}

	public List<Pontuacao> getLstPontuacao() {
		return this.lstPontuacao;
	}

	public void setLstPontuacao(List<Pontuacao> lstPontuacao) {
		this.lstPontuacao = lstPontuacao;
	}

	public Pontuacao addLstPontuacao(Pontuacao lstPontuacao) {
		getLstPontuacao().add(lstPontuacao);
		lstPontuacao.setEstabelecimento(this);

		return lstPontuacao;
	}

	public Pontuacao removeLstPontuacao(Pontuacao lstPontuacao) {
		getLstPontuacao().remove(lstPontuacao);
		lstPontuacao.setEstabelecimento(null);

		return lstPontuacao;
	}

	public List<RegistroPontuacao> getLstRegistroPontuacao() {
		return this.lstRegistroPontuacao;
	}

	public void setLstRegistroPontuacao(List<RegistroPontuacao> lstRegistroPontuacao) {
		this.lstRegistroPontuacao = lstRegistroPontuacao;
	}

	public RegistroPontuacao addLstRegistroPontuacao(RegistroPontuacao lstRegistroPontuacao) {
		getLstRegistroPontuacao().add(lstRegistroPontuacao);
		lstRegistroPontuacao.setEstabelecimento(this);

		return lstRegistroPontuacao;
	}

	public RegistroPontuacao removeLstRegistroPontuacao(RegistroPontuacao lstRegistroPontuacao) {
		getLstRegistroPontuacao().remove(lstRegistroPontuacao);
		lstRegistroPontuacao.setEstabelecimento(null);

		return lstRegistroPontuacao;
	}

}