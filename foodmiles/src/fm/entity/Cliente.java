package fm.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the TBL_CLIENTE database table.
 * 
 */
@Entity
@Table(name="TBL_CLIENTE")
@NamedQuery(name="Cliente.findAll", query="SELECT c FROM Cliente c")
public class Cliente extends fm.entity.gen.GenericEntity<Cliente> implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique=true, nullable=false)
	private int idcliente;

	@Temporal(TemporalType.TIMESTAMP)
	private Date dttinclusao;

	private int numcelular;

	private long numcpf;

	private int numddd;

	@Column(length=255)
	private String stremail;

	@Column(length=255)
	private String strnome;

	@Column(length=255)
	private String strsenha;

	@Column(length=255)
	private String strsobrenome;

	//bi-directional many-to-many association to Empresa
	@ManyToMany(cascade={CascadeType.ALL})
	@JoinTable(
		name="TBL_CLIENTEEMPRESA"
		, joinColumns={
			@JoinColumn(name="IDCLIENTE", nullable=false)
			}
		, inverseJoinColumns={
			@JoinColumn(name="IDEMPRESA", nullable=false)
			}
		)
	private List<Empresa> lstEmpresa;

	//bi-directional many-to-one association to RegistroPontuacao
	@OneToMany(mappedBy="cliente")
	private List<RegistroPontuacao> lstRegistroPontuacao;

	public Cliente() {
	}

	public int getIdcliente() {
		return this.idcliente;
	}

	public void setIdcliente(int idcliente) {
		this.idcliente = idcliente;
	}

	public Date getDttinclusao() {
		return this.dttinclusao;
	}

	public void setDttinclusao(Date dttinclusao) {
		this.dttinclusao = dttinclusao;
	}

	public int getNumcelular() {
		return this.numcelular;
	}

	public void setNumcelular(int numcelular) {
		this.numcelular = numcelular;
	}

	public long getNumcpf() {
		return this.numcpf;
	}

	public void setNumcpf(long numcpf) {
		this.numcpf = numcpf;
	}

	public int getNumddd() {
		return this.numddd;
	}

	public void setNumddd(int numddd) {
		this.numddd = numddd;
	}

	public String getStremail() {
		return this.stremail;
	}

	public void setStremail(String stremail) {
		this.stremail = stremail;
	}

	public String getStrnome() {
		return this.strnome;
	}

	public void setStrnome(String strnome) {
		this.strnome = strnome;
	}

	public String getStrsenha() {
		return this.strsenha;
	}

	public void setStrsenha(String strsenha) {
		this.strsenha = strsenha;
	}

	public String getStrsobrenome() {
		return this.strsobrenome;
	}

	public void setStrsobrenome(String strsobrenome) {
		this.strsobrenome = strsobrenome;
	}

	public List<Empresa> getLstEmpresa() {
		return this.lstEmpresa;
	}

	public void setLstEmpresa(List<Empresa> lstEmpresa) {
		this.lstEmpresa = lstEmpresa;
	}

	public List<RegistroPontuacao> getLstRegistroPontuacao() {
		return this.lstRegistroPontuacao;
	}

	public void setLstRegistroPontuacao(List<RegistroPontuacao> lstRegistroPontuacao) {
		this.lstRegistroPontuacao = lstRegistroPontuacao;
	}

	public RegistroPontuacao addLstRegistroPontuacao(RegistroPontuacao lstRegistroPontuacao) {
		getLstRegistroPontuacao().add(lstRegistroPontuacao);
		lstRegistroPontuacao.setCliente(this);

		return lstRegistroPontuacao;
	}

	public RegistroPontuacao removeLstRegistroPontuacao(RegistroPontuacao lstRegistroPontuacao) {
		getLstRegistroPontuacao().remove(lstRegistroPontuacao);
		lstRegistroPontuacao.setCliente(null);

		return lstRegistroPontuacao;
	}

}