package fm.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the TBL_ESTADO database table.
 * 
 */
@Entity
@Table(name="TBL_ESTADO")
@NamedQuery(name="Estado.findAll", query="SELECT e FROM Estado e")
public class Estado extends fm.entity.gen.GenericEntity<Estado> implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(unique=true, nullable=false)
	private int idestado;

	@Column(length=255)
	private String strdescricao;

	@Column(length=2)
	private String strsigla;

	//bi-directional many-to-one association to Municipio
	@OneToMany(mappedBy="estado")
	private List<Municipio> lstMunicipio;

	public Estado() {
	}

	public int getIdestado() {
		return this.idestado;
	}

	public void setIdestado(int idestado) {
		this.idestado = idestado;
	}

	public String getStrdescricao() {
		return this.strdescricao;
	}

	public void setStrdescricao(String strdescricao) {
		this.strdescricao = strdescricao;
	}

	public String getStrsigla() {
		return this.strsigla;
	}

	public void setStrsigla(String strsigla) {
		this.strsigla = strsigla;
	}

	public List<Municipio> getLstMunicipio() {
		return this.lstMunicipio;
	}

	public void setLstMunicipio(List<Municipio> lstMunicipio) {
		this.lstMunicipio = lstMunicipio;
	}

	public Municipio addLstMunicipio(Municipio lstMunicipio) {
		getLstMunicipio().add(lstMunicipio);
		lstMunicipio.setEstado(this);

		return lstMunicipio;
	}

	public Municipio removeLstMunicipio(Municipio lstMunicipio) {
		getLstMunicipio().remove(lstMunicipio);
		lstMunicipio.setEstado(null);

		return lstMunicipio;
	}

}