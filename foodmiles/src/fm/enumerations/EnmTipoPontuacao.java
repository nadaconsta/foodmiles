package fm.enumerations;

public enum EnmTipoPontuacao {
	Visitas("VST", "pontuacao.enmTipo.visitas"), Valor("VAL", "pontuacao.enmTipo.valor");

	private final String value;
	private final String desc;

	EnmTipoPontuacao(String value, String desc) {
		this.value = value;
		this.desc = desc;
	}

	public String value() {
		return value;
	}

	public String desc() {
		return desc;
	}

}
