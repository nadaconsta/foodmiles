package fm.services;

import java.text.ParseException;
import java.util.List;

import fm.entity.Cliente;
import fm.entity.Estabelecimento;
import fm.entity.Funcionario;
import fm.entity.RegistroPontuacao;

public interface GenerateService extends GenericService {

	List<Cliente> generateListClienteByFrequency(Estabelecimento estabelecimento)
			throws ParseException;

	String generateNomeEmpresaCliente(Cliente cliente,
			Estabelecimento estabelecimento);

	List<RegistroPontuacao> generateListRegistroPontuacaoRecenteByFuncionario(
			Funcionario funcionario);

	List<RegistroPontuacao> generateListRegistroPontuacaoRequestedByEstabelecimento(
			Estabelecimento estabelecimento);

	List<RegistroPontuacao> generateListRegistroPontuacaoApprovedByEstabelecimento(
			Estabelecimento estabelecimento);

	List<RegistroPontuacao> generateListRegistroPontuacaoRejectedByEstabelecimento(
			Estabelecimento estabelecimento);

	List<Funcionario> generateListFuncionarioByEstabelecimento(
			Estabelecimento estabelecimento);

}
