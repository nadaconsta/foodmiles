package fm.services;

import fm.entity.Cliente;
import fm.entity.Estabelecimento;
import fm.entity.Funcionario;
import fm.entity.RegistroPontuacao;

public interface UpdateService extends GenericService {

	Estabelecimento updateEstabelecimento(Estabelecimento estabelecimento);

	Cliente updateCliente(Cliente cliente);

	RegistroPontuacao updateRegistroPontuacao(RegistroPontuacao registroPontuacao);

	Funcionario updateFuncionario(Funcionario funcionario);

}
