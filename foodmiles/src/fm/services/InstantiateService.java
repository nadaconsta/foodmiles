package fm.services;

import fm.entity.Cliente;
import fm.entity.Empresa;
import fm.entity.Estabelecimento;
import fm.entity.Funcionario;
import fm.entity.RegistroPontuacao;

public interface InstantiateService extends GenericService {

	Empresa instantiateEmpresa(String nomeEmpresa);

	Cliente instantiateCliente(Long numCpf);

	Funcionario instantiateFuncionario(Long numCpf);

	Estabelecimento instantiateEstabelecimento(Long numCnpj);

	RegistroPontuacao instantiateRegistroPontuacao();

}
