package fm.services;

import java.text.ParseException;
import java.util.List;

import javax.persistence.NoResultException;

import fm.entity.Cliente;
import fm.entity.Empresa;
import fm.entity.Estabelecimento;
import fm.entity.Funcionario;
import fm.entity.Pontuacao;
import fm.entity.gen.GenericEntity;

public interface ReadService extends GenericService {

	<T> GenericEntity<T> readById(Integer id, Class<T> class_);

	<T> GenericEntity<T> refresh(GenericEntity<T> entity);

	void refreshEntityManager();

	Estabelecimento readEstabelecimentoByCNPJSenha(Long CNPJ, String senha)
			throws NoResultException;

	Funcionario readFuncionarioByCPFSenha(Long CPF, String Senha)
			throws NoResultException;

	Cliente readClienteByCPF(Long CPF) throws NoResultException;

	List<String> readListClienteCPFByPartialCPF(String value)
			throws ParseException, NoResultException;

	List<String> readListClienteNomeSobrenomeByPartialNomeSobrenome(
			String value, Estabelecimento estabelecimento)
			throws ParseException, NoResultException;

	Empresa readEmpresaByNome(String nomeEmpresa);

	Pontuacao readValidPontuacaoByIdEstabelecimento(int idestabelecimento);

	Funcionario readFuncionarioByCPFSenhaNull(Long cpf);

}
