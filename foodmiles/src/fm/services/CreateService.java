package fm.services;

import fm.entity.Cliente;
import fm.entity.Empresa;
import fm.entity.RegistroPontuacao;

public interface CreateService extends GenericService {

	Empresa createEmpresa(String nomeEmpresa);
	
	Cliente createCliente(Long numCpf);
	
	Cliente createCliente(Cliente cliente);

	RegistroPontuacao createRegistroPontuacao(RegistroPontuacao registroPontuacao);

}
