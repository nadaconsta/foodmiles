package fm.services;

import java.util.Date;

import fm.entity.Cliente;
import fm.entity.Estabelecimento;
import fm.entity.Pontuacao;
import fm.entity.RegistroPontuacao;

public interface CalculateService extends GenericService {

	int calcPontos(Pontuacao pontuacao, RegistroPontuacao registroPontuacao);

	int calcMediaVisitas(Cliente cliente, Estabelecimento estabelecimento,
			boolean limit);

	int calcSaldoPontos(Cliente cliente, Estabelecimento estabelecimento);

	Date calcDate(Date baseDate, int calendarType, int amount);

	Date calcUltimaVisita(Cliente cliente, Estabelecimento estabelecimento);

}
