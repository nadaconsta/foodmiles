package fm.services;

public interface MessageService extends GenericService {

	String getMessage(String code);

	String getMessage(String code, Object[] params);

}
