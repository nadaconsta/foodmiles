package fm.services;

import java.security.NoSuchAlgorithmException;

import fm.entity.Cliente;

public interface ValidateService extends GenericService {

	boolean isValidCPF(Long cpf);

	boolean isValidCNPJ(Long cnpj);

	boolean isClienteCompletlyRegistered(Cliente cliente);

	boolean isValidEmail(String email);

	boolean isValidCelular(Integer ddd, Integer celular);

	boolean isValidTelefone(Integer ddd, Integer strTelefone);

	boolean isValidPassword(String password);

	boolean confirmPassword(String password, String confirm);

	String encryptPassword(String pass) throws NoSuchAlgorithmException;

}
