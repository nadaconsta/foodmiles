package fm.services;

import java.text.ParseException;

public interface FormatService extends GenericService {
	String formatString(String value, String mask) throws ParseException;

	String formatCPFString(Long value) throws ParseException;

	Long formatCPFLong(String value) throws ParseException;

	String formatCNPJString(Long numcnpj) throws ParseException;

	Long formatCNPJLong(String strcnpj);

	String fillRight(String input, String fill, int finalSize);

	String fillLeft(String input, String fill, int finalSize);

	String extractBracketedValue(String nomeSobrenome);

	String removeBracketedValue(String input);

	String formatTelefoneToString(String value) throws ParseException;

	String formatBooleanToString(Boolean value) throws ParseException;

	Integer extractTelefone(String strTelefone);

	Integer extractCelular(String strTelefone);

	Integer extractDDD(String strTelefone);

}
