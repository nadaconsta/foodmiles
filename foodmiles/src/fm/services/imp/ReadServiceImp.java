package fm.services.imp;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.persistence.NoResultException;

import fm.constants.Constants;
import fm.dao.ClienteDao;
import fm.dao.EmpresaDao;
import fm.dao.EstabelecimentoDao;
import fm.dao.FuncionarioDao;
import fm.dao.GenericDao;
import fm.dao.PontuacaoDao;
import fm.entity.Cliente;
import fm.entity.Empresa;
import fm.entity.Estabelecimento;
import fm.entity.Funcionario;
import fm.entity.Pontuacao;
import fm.entity.RegistroPontuacao;
import fm.entity.gen.GenericEntity;
import fm.services.FormatService;
import fm.services.ReadService;

@ApplicationScoped
@ManagedBean(eager = true)
public class ReadServiceImp extends GenericServiceImp implements ReadService {
	EstabelecimentoDao estabelecimentoDao;
	FuncionarioDao funcionarioDao;
	PontuacaoDao pontuacaoDao;
	ClienteDao clienteDao;
	EmpresaDao empresaDao;
	GenericDao genericDao;

	FormatService formatService;

	@Override
	@PostConstruct
	public void init() {
		estabelecimentoDao = super.findBean(EstabelecimentoDao.class);
		funcionarioDao = super.findBean(FuncionarioDao.class);
		pontuacaoDao = super.findBean(PontuacaoDao.class);
		clienteDao = super.findBean(ClienteDao.class);
		empresaDao = super.findBean(EmpresaDao.class);
		genericDao = super.findBean(GenericDao.class);
		formatService = super.findBean(FormatService.class);
	}

	@Override
	public List<String> readListClienteCPFByPartialCPF(String value)
			throws ParseException {
		Long inputNum = formatService.formatCPFLong(value);
		List<String> lsReturn = new ArrayList<String>();
		if (inputNum != Constants.ZERO_INT) {
			String sBegin = formatService.fillRight(inputNum.toString(),
					Constants.ZERO_STRING, Constants.CPF_SIZE);
			Long inputNumEnd = inputNum + 1;
			String sEnd = formatService.fillRight(inputNumEnd.toString(),
					Constants.ZERO_STRING, Constants.CPF_SIZE);

			inputNum = Long.valueOf(sBegin);
			inputNumEnd = Long.valueOf(sEnd);

			List<Cliente> lsCliente = clienteDao.findClienteByRangeCPF(
					inputNum, inputNumEnd);
			for (Cliente cliente : lsCliente) {
				try {
					String formatedCPF = formatService.formatCPFString(cliente
							.getNumcpf());
					lsReturn.add(formatedCPF);
				} catch (ParseException e) {
					throw e;
				}
			}
		}
		return lsReturn;
	}

	@Override
	public List<String> readListClienteNomeSobrenomeByPartialNomeSobrenome(
			String value, Estabelecimento estabelecimento)
			throws ParseException, NoResultException {

		List<Cliente> lsCliente = clienteDao.findClienteByNomeSobrenome(value);

		List<String> lsPriority = new ArrayList<String>();
		List<String> lsOther = new ArrayList<String>();
		boolean added = false;

		for (Cliente cliente : lsCliente) {
			try {

				String formatedCPF = formatService.formatCPFString(cliente
						.getNumcpf());
				for (RegistroPontuacao registroPontuacao : cliente
						.getLstRegistroPontuacao()) {
					if (registroPontuacao.getEstabelecimento().equals(
							estabelecimento)) {
						lsPriority.add(cliente.getStrnome() + Constants.SPACE
								+ cliente.getStrsobrenome() + Constants.SPACE
								+ Constants.OPEN_BRACKET + formatedCPF
								+ Constants.CLOSE_BRACKET);
						added = true;
						break;
					}
				}
				if (!added) {
					lsOther.add(cliente.getStrnome() + Constants.SPACE
							+ cliente.getStrsobrenome() + Constants.SPACE
							+ Constants.OPEN_BRACKET + formatedCPF
							+ Constants.CLOSE_BRACKET);
				}
			} catch (ParseException e) {
				throw e;
			}
		}
		lsPriority.addAll(lsOther);
		return lsPriority;
	}

	@Override
	public <T> GenericEntity<T> readById(Integer id, Class<T> class_) {
		return genericDao.findById(id, class_);
	}

	@Override
	public <T> GenericEntity<T> refresh(GenericEntity<T> entity) {
		return genericDao.refresh(entity);
	}

	@Override
	public Estabelecimento readEstabelecimentoByCNPJSenha(Long cnpj,
			String senha) {
		return estabelecimentoDao.findEstabelecimentoByCNPJSenha(cnpj, senha);
	}

	@Override
	public Funcionario readFuncionarioByCPFSenha(Long cpf, String senha) {
		return funcionarioDao.findFuncionarioByCPFSenha(cpf, senha);
	}

	@Override
	public Funcionario readFuncionarioByCPFSenhaNull(Long cpf) {
		return funcionarioDao.findFuncionarioByCPFSenhaNull(cpf);
	}

	@Override
	public Cliente readClienteByCPF(Long cpf) {
		return clienteDao.findClienteByCPF(cpf);
	}

	@Override
	public Empresa readEmpresaByNome(String nome) {
		return empresaDao.findEmpresaByNome(nome);
	}

	@Override
	public Pontuacao readValidPontuacaoByIdEstabelecimento(int idestabelecimento) {
		return pontuacaoDao
				.findValidPontuacaoByIdEstabelecimento(idestabelecimento);
	}

	@Override
	public void refreshEntityManager() {
		genericDao.refreshEntityManager();
	}

}
