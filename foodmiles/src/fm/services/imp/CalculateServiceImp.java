package fm.services.imp;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

import fm.constants.Constants;
import fm.entity.Cliente;
import fm.entity.Estabelecimento;
import fm.entity.Pontuacao;
import fm.entity.RegistroPontuacao;
import fm.enumerations.EnmTipoPontuacao;
import fm.services.CalculateService;

@ApplicationScoped
@ManagedBean(eager = true)
public class CalculateServiceImp extends GenericServiceImp implements
		CalculateService {

	@Override
	@PostConstruct
	public void init() {
	}

	@Override
	public int calcPontos(Pontuacao pontuacao,
			RegistroPontuacao registroPontuacao) {
		if (registroPontuacao.isFlggerarpontos()) {
			if (EnmTipoPontuacao.Valor.value().equals(pontuacao.getEnmtipo())
					&& pontuacao.getNumvalor() > Constants.ZERO_INT) {
				Double pontos = registroPontuacao.getValconta()
						/ pontuacao.getNumvalor();
				return pontos.intValue();
			} else if (EnmTipoPontuacao.Visitas.value().equals(
					pontuacao.getEnmtipo())) {
				return pontuacao.getNumvalor();
			}
		}
		return 0;
	}

	@Override
	public int calcMediaVisitas(Cliente cliente,
			Estabelecimento estabelecimento, boolean limit) {
		List<RegistroPontuacao> lstRegistroPontuacao = cliente
				.getLstRegistroPontuacao();
		int contVisitas = 0;
		Date dataCorte = null;
		if (limit) {
			dataCorte = this.calcDate(new Date(), Calendar.MONTH,
					Constants.MESES_RECENTES);
		}
		for (RegistroPontuacao registroPontuacao : lstRegistroPontuacao) {
			if (estabelecimento.equals(registroPontuacao.getEstabelecimento())) {
				if (limit) {
					if (registroPontuacao.getDttreferencia().after(dataCorte)
							&& registroPontuacao.getValconta() > Constants.ZERO_INT) {
						contVisitas++;
					}
				} else {
					if (registroPontuacao.getValconta() > Constants.ZERO_INT) {
						contVisitas++;
					}
				}
			}
		}
		return contVisitas;
	}

	@Override
	public int calcSaldoPontos(Cliente cliente, Estabelecimento estabelecimento) {
		List<RegistroPontuacao> lstRegistroPontuacao = cliente
				.getLstRegistroPontuacao();
		int sumPontos = 0;
		for (RegistroPontuacao registroPontuacao : lstRegistroPontuacao) {
			if (estabelecimento.equals(registroPontuacao.getEstabelecimento())) {
				if (registroPontuacao.isFlgestornoaprovado()
						|| registroPontuacao.isFlgestornosolicitado()) {
					// N�o calcular os pontos para os registros estornados,
					// sejam eles solicitados ou aprovados.
				} else {
					sumPontos += registroPontuacao.getNumpontos();
				}
			}
		}
		return sumPontos;
	}

	@Override
	public Date calcDate(Date baseDate, int calendarType, int amount) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(baseDate);
		cal.add(calendarType, amount);
		return cal.getTime();
	}

	@Override
	public Date calcUltimaVisita(Cliente cliente, Estabelecimento estabelecimento) {
		List<RegistroPontuacao> lstRegistroPontuacao = cliente
				.getLstRegistroPontuacao();
		Date dataUltimaVisita = null;
		for (RegistroPontuacao registroPontuacao : lstRegistroPontuacao) {
			if (estabelecimento.equals(registroPontuacao.getEstabelecimento())) {
				if (dataUltimaVisita == null ||(registroPontuacao.getDttreferencia().after(dataUltimaVisita)
						&& registroPontuacao.getValconta() > Constants.ZERO_INT)) {
					dataUltimaVisita = registroPontuacao.getDttreferencia();
				}
			}
		}
		return dataUltimaVisita;
	}

}
