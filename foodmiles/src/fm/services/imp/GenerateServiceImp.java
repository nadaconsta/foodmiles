package fm.services.imp;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

import fm.constants.Constants;
import fm.entity.Cliente;
import fm.entity.Empresa;
import fm.entity.Estabelecimento;
import fm.entity.Funcionario;
import fm.entity.RegistroPontuacao;
import fm.services.CalculateService;
import fm.services.FormatService;
import fm.services.GenerateService;

@ApplicationScoped
@ManagedBean(eager = true)
public class GenerateServiceImp extends GenericServiceImp implements
		GenerateService {
	FormatService formatService;
	CalculateService calculateService;

	@Override
	@PostConstruct
	public void init() {
		formatService = super.findBean(FormatService.class);
		calculateService = super.findBean(CalculateService.class);
	}

	@Override
	public List<Cliente> generateListClienteByFrequency(
			Estabelecimento estabelecimento) throws ParseException {
		List<Cliente> lstRetorno = new ArrayList<Cliente>();
		List<RegistroPontuacao> lstRegistroPontuacao = estabelecimento
				.getLstRegistroPontuacao();
		Date dataCorte = calculateService.calcDate(new Date(), Calendar.MONTH,
				Constants.MESES_RECENTES);

		for (RegistroPontuacao registroPontuacao : lstRegistroPontuacao) {
			Cliente cliente = registroPontuacao.getCliente();
			if (lstRetorno.contains(cliente)) {
				if (registroPontuacao.getValconta() > Constants.ZERO_INT
						&& registroPontuacao.getDttreferencia()
								.after(dataCorte)) {
					int qttVisitas = cliente.getNumber();
					cliente.setNumber(++qttVisitas);
				}
			} else {
				if (registroPontuacao.getValconta() > Constants.ZERO_INT
						&& registroPontuacao.getDttreferencia()
								.after(dataCorte)) {
					cliente.setNumber(1);
				} else {
					cliente.setNumber(0);
				}
				cliente.setDescription(this.generateNomeEmpresaCliente(cliente,
						estabelecimento));
				cliente.setValue((double) calculateService.calcSaldoPontos(
						cliente, estabelecimento));
				lstRetorno.add(cliente);
			}
		}
		this.sortClienteByNumber(lstRetorno);

		return lstRetorno;
	}

	@Override
	public String generateNomeEmpresaCliente(Cliente cliente,
			Estabelecimento estabelecimento) {
		List<Empresa> lstEmpresaCliente = cliente.getLstEmpresa();
		if (lstEmpresaCliente != null) {
			for (Empresa empresaCliente : lstEmpresaCliente) {
				if (estabelecimento.getLstEmpresa().contains(empresaCliente)) {
					return empresaCliente.getStrnome();
				}
			}
			if (!lstEmpresaCliente.isEmpty()) {
				return lstEmpresaCliente.get(Constants.ZERO_INT).getStrnome();
			}
		}
		return Constants.EMPTY_STRING;
	}

	@Override
	public List<RegistroPontuacao> generateListRegistroPontuacaoRecenteByFuncionario(
			Funcionario funcionario) {
		List<RegistroPontuacao> lstRetorno = new ArrayList<RegistroPontuacao>();
		List<RegistroPontuacao> lstRegistroPontuacao = funcionario
				.getLstRegistroPontuacao();
		Date dataCorte = calculateService.calcDate(new Date(), Calendar.MONTH,
				Constants.MESES_RECENTES);
		for (RegistroPontuacao registroPontuacao : lstRegistroPontuacao) {
			if (registroPontuacao.getDttinclusao().after(dataCorte)) {
				registroPontuacao.setSelected(true);
				if (registroPontuacao.isFlgestornoaprovado()) {
					registroPontuacao.setDescription(getMsgsBundle().getString(
							Constants.APPROVED));
				} else if (registroPontuacao.isFlgestornoreprovado()) {
					registroPontuacao.setDescription(getMsgsBundle().getString(
							Constants.REJECTED));
				} else if (registroPontuacao.isFlgestornosolicitado()) {
					registroPontuacao.setDescription(getMsgsBundle().getString(
							Constants.REQUESTED));
				} else {
					registroPontuacao.setSelected(false);
				}

				lstRetorno.add(registroPontuacao);
			}
		}
		sortRegistroPontuacaoByDttInclusao(lstRetorno);
		return lstRetorno;
	}

	@Override
	public List<RegistroPontuacao> generateListRegistroPontuacaoRequestedByEstabelecimento(
			Estabelecimento estabelecimento) {
		List<RegistroPontuacao> lstRetorno = new ArrayList<RegistroPontuacao>();
		List<RegistroPontuacao> lstRegistroPontuacao = estabelecimento
				.getLstRegistroPontuacao();
		Date dataCorte = calculateService.calcDate(new Date(), Calendar.MONTH,
				Constants.MESES_RECENTES);
		for (RegistroPontuacao registroPontuacao : lstRegistroPontuacao) {
			if (registroPontuacao.getDttinclusao().after(dataCorte)
					&& registroPontuacao.isFlgestornosolicitado()) {
				lstRetorno.add(registroPontuacao);
			}
		}

		sortRegistroPontuacaoByDttInclusao(lstRetorno);
		return lstRetorno;
	}

	@Override
	public List<RegistroPontuacao> generateListRegistroPontuacaoApprovedByEstabelecimento(
			Estabelecimento estabelecimento) {
		List<RegistroPontuacao> lstRetorno = new ArrayList<RegistroPontuacao>();
		List<RegistroPontuacao> lstRegistroPontuacao = estabelecimento
				.getLstRegistroPontuacao();
		Date dataCorte = calculateService.calcDate(new Date(), Calendar.MONTH,
				Constants.MESES_RECENTES);
		for (RegistroPontuacao registroPontuacao : lstRegistroPontuacao) {
			if (registroPontuacao.getDttinclusao().after(dataCorte)
					&& registroPontuacao.isFlgestornoaprovado()) {
				lstRetorno.add(registroPontuacao);
			}
		}

		sortRegistroPontuacaoByDttInclusao(lstRetorno);
		return lstRetorno;
	}

	@Override
	public List<RegistroPontuacao> generateListRegistroPontuacaoRejectedByEstabelecimento(
			Estabelecimento estabelecimento) {
		List<RegistroPontuacao> lstRetorno = new ArrayList<RegistroPontuacao>();
		List<RegistroPontuacao> lstRegistroPontuacao = estabelecimento
				.getLstRegistroPontuacao();
		Date dataCorte = calculateService.calcDate(new Date(), Calendar.MONTH,
				Constants.MESES_RECENTES);
		for (RegistroPontuacao registroPontuacao : lstRegistroPontuacao) {
			if (registroPontuacao.getDttinclusao().after(dataCorte)
					&& registroPontuacao.isFlgestornoreprovado()) {
				lstRetorno.add(registroPontuacao);
			}
		}

		sortRegistroPontuacaoByDttInclusao(lstRetorno);
		return lstRetorno;
	}

	@Override
	public List<Funcionario> generateListFuncionarioByEstabelecimento(
			Estabelecimento estabelecimento) {
		return estabelecimento.getLstFuncionario();
	}

	// The Java 8 lambda expression cannot exist twice in the managed bean class
	// or it fails to instantiate the applicationscoped annotation.
	private void sortClienteByNumber(List<Cliente> lstRetorno) {
		lstRetorno.sort((c1, c2) -> c2.getNumber().compareTo(c1.getNumber()));
	}

	private void sortRegistroPontuacaoByDttInclusao(
			List<RegistroPontuacao> lstRetorno) {
		lstRetorno.sort((c1, c2) -> c2.getDttinclusao().compareTo(
				c1.getDttinclusao()));
	}
}
