package fm.services.imp;

import java.text.MessageFormat;
import java.util.ResourceBundle;

import javax.annotation.PostConstruct;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;

import fm.constants.ConfigConstants;
import fm.services.MessageService;

@ApplicationScoped
@ManagedBean(eager = true)
public class MessageServiceImp extends GenericServiceImp implements MessageService {
	private ResourceBundle msgsBundle;

	@Override
	@PostConstruct
	public void init() {
		msgsBundle = FacesContext.getCurrentInstance().getApplication()
				.getResourceBundle(FacesContext.getCurrentInstance(), ConfigConstants.MSGS_PROPERTIES);
	}

	@Override
	public String getMessage(String code) {
		return msgsBundle.getString(code);
	}
	
	@Override
	public String getMessage(String code, Object params[]) {
		if (params != null) {
            MessageFormat mf = new MessageFormat(msgsBundle.getString(code));
            return mf.format(params, new StringBuffer(), null).toString();
        }
		return msgsBundle.getString(code);
	}
}
