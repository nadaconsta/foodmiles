package fm.services.imp;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

import fm.managed.bean.imp.GenericManagedBeanImp;
import fm.services.GenericService;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

@ApplicationScoped
@ManagedBean(eager = true)
// This is the class used to implement one method to all services
public class GenericServiceImp extends GenericManagedBeanImp implements
		GenericService {

	@Override
	public void init() {
		throw new NotImplementedException();
	}

}
