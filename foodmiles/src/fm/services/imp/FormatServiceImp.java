package fm.services.imp;

import java.text.ParseException;

import javax.annotation.PostConstruct;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.swing.text.MaskFormatter;

import fm.constants.Constants;
import fm.services.FormatService;

@ApplicationScoped
@ManagedBean(eager = true)
public class FormatServiceImp extends GenericServiceImp implements
		FormatService {

	@Override
	@PostConstruct
	public void init() {
	}

	private String formatStringToOnlyNumbers(String value) {
		value = value.replaceAll("[^\\d]", "");
		if (value.isEmpty()) {
			return Constants.ZERO_STRING;
		} else {
			return value;
		}
	}

	@Override
	public String formatString(String value, String mask) throws ParseException {
		/* ******************************************************************************************
		 * Mask Character Description 
		 * 
		 * # Any valid number, uses Character.isDigit. 
		 * ' Escape character, used to escape any of the special formatting characters. 
		 * U Any character (Character.isLetter). All lower case letters are mapped to upper case. 
		 * L Any character (Character.isLetter). All upper case letters are mapped to lower case. 
		 * A Any character or number (Character.isLetter or Character.isDigit) 
		 * ? Any character (Character.isLetter). 
		 * * Anything.
		 * H Any hex character (0-9, a-f or A-F).
		 * ******************************************************************************************/
		MaskFormatter mf = new MaskFormatter(mask);
		mf.setValueContainsLiteralCharacters(false);
		return mf.valueToString(value);
	}

	@Override
	public String fillRight(String input, String fill, int finalSize) {
		while (input.length() < finalSize) {
			input = input + fill;
		}
		return input;
	}

	@Override
	public String fillLeft(String input, String fill, int finalSize) {
		while (input.length() < finalSize) {
			input = fill + input;
		}
		return input;
	}

	@Override
	public String extractBracketedValue(String input) {
		if (!input.contains(Constants.OPEN_BRACKET)
				&& !input.contains(Constants.CLOSE_BRACKET)) {
			return input;
		} else {
			return input.substring(input.indexOf(Constants.OPEN_BRACKET),
					input.indexOf(Constants.CLOSE_BRACKET));
		}
	}

	@Override
	public String removeBracketedValue(String input) {
		if (!input.contains(Constants.OPEN_BRACKET)
				&& !input.contains(Constants.CLOSE_BRACKET)) {
			return input;
		} else {
			String ret1 = input.substring(0,
					input.indexOf(Constants.OPEN_BRACKET));
			String ret2 = input.substring(
					input.indexOf(Constants.CLOSE_BRACKET), input.length());
			return (ret1 + ret2).trim();
		}
	}

	@Override
	public Integer extractTelefone(String strTelefone) {
		String telefone = this.formatStringToOnlyNumbers(this
				.removeBracketedValue(strTelefone));
		return Integer.valueOf(telefone);
	}

	@Override
	public Integer extractCelular(String strCelular) {
		String celular = this.formatStringToOnlyNumbers(this
				.removeBracketedValue(strCelular));
		return Integer.valueOf(celular);
	}

	@Override
	public Integer extractDDD(String strTelefone) {
		String dddTelefone = this.formatStringToOnlyNumbers(this
				.extractBracketedValue(strTelefone));
		return Integer.valueOf(dddTelefone);
	}

	@Override
	public String formatCPFString(Long value) throws ParseException {
		String cpf = this.fillLeft(value.toString(), Constants.ZERO_STRING,
				Constants.CPF_SIZE);
		return this.formatString(cpf, Constants.MASK_CPF);
	}

	@Override
	public String formatTelefoneToString(String value) throws ParseException {
		if (value.length() == Constants.DDD_TELEFONE_LENGHT) {
			return this.formatString(value, Constants.MASK_TELEFONE);
		} else if (value.length() == Constants.DDD_CELULAR_LENGHT) {
			return this.formatString(value, Constants.MASK_CELULAR);
		} else {
			return null;
		}
	}

	@Override
	public String formatBooleanToString(Boolean value) throws ParseException {
		if (value) {
			return getMsgsBundle().getString(Constants.YES);
		} else {
			return getMsgsBundle().getString(Constants.NO);
		}
	}

	@Override
	public Long formatCPFLong(String value) throws ParseException {
		String numValue = this.formatStringToOnlyNumbers(value);
		return Long.parseLong(numValue);
	}

	@Override
	public String formatCNPJString(Long value) throws ParseException {
		String cnpj = this.fillLeft(value.toString(), Constants.ZERO_STRING,
				Constants.CNPJ_SIZE);
		return this.formatString(cnpj, Constants.MASK_CNPJ);
	}

	@Override
	public Long formatCNPJLong(String value) {
		String numValue = this.formatStringToOnlyNumbers(value);
		return Long.parseLong(numValue);
	}
}
