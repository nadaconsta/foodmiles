package fm.services.imp;

import java.util.ArrayList;
import java.util.Date;

import javax.annotation.PostConstruct;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

import fm.dao.ClienteDao;
import fm.dao.EmpresaDao;
import fm.dao.RegistroPontuacaoDao;
import fm.entity.Cliente;
import fm.entity.Empresa;
import fm.entity.Estabelecimento;
import fm.entity.RegistroPontuacao;
import fm.services.CreateService;

@ApplicationScoped
@ManagedBean(eager = true)
public class CreateServiceImp extends GenericServiceImp implements
		CreateService {

	EmpresaDao empresaDao;
	ClienteDao clienteDao;
	RegistroPontuacaoDao registroPontuacaoDao;

	@Override
	@PostConstruct
	public void init() {
		empresaDao = super.findBean(EmpresaDao.class);
		clienteDao = super.findBean(ClienteDao.class);
		registroPontuacaoDao = super.findBean(RegistroPontuacaoDao.class);
	}

	@Override
	public Empresa createEmpresa(String nomeEmpresa) {
		Empresa empresa = new Empresa();
		empresa.setStrnome(nomeEmpresa);
		empresa.setDttinclusao(new Date());
		empresa.setLstCliente(new ArrayList<Cliente>());
		empresa.setLstEstabelecimento(new ArrayList<Estabelecimento>());
		return empresaDao.createEmpresa(empresa);
	}

	@Override
	public Cliente createCliente(Long numCpf) {
		Cliente cliente = new Cliente();
		cliente.setNumcpf(numCpf);
		cliente.setDttinclusao(new Date());
		cliente.setLstRegistroPontuacao(new ArrayList<RegistroPontuacao>());
		cliente.setLstEmpresa(new ArrayList<Empresa>());
		return clienteDao.createCliente(cliente);
	}

	@Override
	public Cliente createCliente(Cliente cliente) {
		return clienteDao.createCliente(cliente);
	}

	@Override
	public RegistroPontuacao createRegistroPontuacao(
			RegistroPontuacao registroPontuacao) {
		return registroPontuacaoDao.createRegistroPontuacao(registroPontuacao);
	}

}