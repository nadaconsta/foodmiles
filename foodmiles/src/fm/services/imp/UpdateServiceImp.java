package fm.services.imp;

import javax.annotation.PostConstruct;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

import fm.dao.ClienteDao;
import fm.dao.EstabelecimentoDao;
import fm.dao.FuncionarioDao;
import fm.dao.RegistroPontuacaoDao;
import fm.entity.Cliente;
import fm.entity.Estabelecimento;
import fm.entity.Funcionario;
import fm.entity.RegistroPontuacao;
import fm.services.UpdateService;

@ApplicationScoped
@ManagedBean(eager = true)
public class UpdateServiceImp extends GenericServiceImp implements UpdateService {

	EstabelecimentoDao estabelecimentoDao;
	ClienteDao clienteDao;
	FuncionarioDao funcionarioDao;
	RegistroPontuacaoDao registroPontuacaoDao;

	@Override
	@PostConstruct
	public void init() {
		estabelecimentoDao = super.findBean(EstabelecimentoDao.class);
		clienteDao = super.findBean(ClienteDao.class);
		funcionarioDao = super.findBean(FuncionarioDao.class);
		registroPontuacaoDao = super.findBean(RegistroPontuacaoDao.class);
	}

	@Override
	public Estabelecimento updateEstabelecimento(Estabelecimento estabelecimento) {
		return estabelecimentoDao.updateEstabelecimento(estabelecimento);
	}

	@Override
	public Cliente updateCliente(Cliente cliente) {
		return clienteDao.updateCliente(cliente);
	}

	@Override
	public RegistroPontuacao updateRegistroPontuacao(RegistroPontuacao registroPontuacao) {
		return registroPontuacaoDao.updateRegistroPontuacao(registroPontuacao);
	}

	@Override
	public Funcionario updateFuncionario(Funcionario funcionario) {
		return funcionarioDao.updateFuncionario(funcionario);
	}
}
