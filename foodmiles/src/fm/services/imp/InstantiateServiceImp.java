package fm.services.imp;

import java.util.ArrayList;
import java.util.Date;

import javax.annotation.PostConstruct;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

import fm.entity.Cliente;
import fm.entity.Empresa;
import fm.entity.Estabelecimento;
import fm.entity.Funcionario;
import fm.entity.RegistroPontuacao;
import fm.services.InstantiateService;

@ApplicationScoped
@ManagedBean(eager = true)
public class InstantiateServiceImp extends GenericServiceImp implements
		InstantiateService {

	@Override
	@PostConstruct
	public void init() {
	}

	@Override
	public Empresa instantiateEmpresa(String nomeEmpresa) {
		Empresa empresa = new Empresa();
		empresa.setStrnome(nomeEmpresa);
		empresa.setDttinclusao(new Date());
		empresa.setLstCliente(new ArrayList<Cliente>());
		empresa.setLstEstabelecimento(new ArrayList<Estabelecimento>());
		return empresa;
	}

	@Override
	public Cliente instantiateCliente(Long numCpf) {
		Cliente cliente = new Cliente();
		cliente.setNumcpf(numCpf);
		cliente.setDttinclusao(new Date());
		cliente.setLstRegistroPontuacao(new ArrayList<RegistroPontuacao>());
		cliente.setLstEmpresa(new ArrayList<Empresa>());
		return cliente;
	}

	@Override
	public Estabelecimento instantiateEstabelecimento(Long numCnpj) {
		Estabelecimento estabelecimento = new Estabelecimento();
		estabelecimento.setNumcnpj(numCnpj);
		estabelecimento.setDttinclusao(new Date());
		estabelecimento
				.setLstRegistroPontuacao(new ArrayList<RegistroPontuacao>());
		estabelecimento.setLstEmpresa(new ArrayList<Empresa>());
		return estabelecimento;
	}

	@Override
	public Funcionario instantiateFuncionario(Long numCpf) {
		Funcionario funcionario = new Funcionario();
		funcionario.setNumcpf(numCpf);
		funcionario.setDttinclusao(new Date());
		funcionario.setLstRegistroPontuacao(new ArrayList<RegistroPontuacao>());
		funcionario.setFlgativo(true);
		return funcionario;
	}

	@Override
	public RegistroPontuacao instantiateRegistroPontuacao() {
		RegistroPontuacao registroPontuacao = new RegistroPontuacao();
		registroPontuacao.setDttinclusao(new Date());
		registroPontuacao.setDttreferencia(new Date());
		registroPontuacao.setFlgestornoreprovado(false);
		registroPontuacao.setFlgestornoaprovado(false);
		registroPontuacao.setFlgestornosolicitado(false);
		registroPontuacao.setFlggerarpontos(true);
		return registroPontuacao;
	}

}
