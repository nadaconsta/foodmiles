package fm.services.imp;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

import fm.constants.Constants;
import fm.entity.Cliente;
import fm.services.FormatService;
import fm.services.ValidateService;

@ApplicationScoped
@ManagedBean(eager = true)
public class ValidateServiceImp extends GenericServiceImp implements
ValidateService {

	public static final int[] pesoCPF = { 11, 10, 9, 8, 7, 6, 5, 4, 3, 2 };
	public static final int[] pesoCNPJ = { 6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3,
			2 };

	// Cryptografy
	private static MessageDigest md;

	// Reg Exp patterns
	private Pattern email_pattern;
	private Pattern telefone_pattern;
	private Pattern celular8_pattern;
	private Pattern celular9_pattern;
	private Pattern password_pattern;

	FormatService formatService;

	@Override
	@PostConstruct
	public void init() {
		formatService = super.findBean(FormatService.class);

		email_pattern = Pattern.compile(Constants.EMAIL_PATTERN);
		telefone_pattern = Pattern.compile(Constants.TELEFONE_PATTERN);
		celular8_pattern = Pattern.compile(Constants.CELULAR_8_PATTERN);
		celular9_pattern = Pattern.compile(Constants.CELULAR_9_PATTERN);
		password_pattern = Pattern.compile(Constants.PASSWORD_PATTERN);
	}

	private static int calcularDigito(String str, int[] peso) {
		int soma = 0;
		for (int indice = str.length() - 1, digito; indice >= 0; indice--) {
			digito = Integer.parseInt(str.substring(indice, indice + 1));
			soma += digito * peso[peso.length - str.length() + indice];
		}
		soma = 11 - soma % 11;
		return soma > 9 ? 0 : soma;
	}

	@Override
	public boolean isValidCPF(Long numcpf) {
		if (numcpf == null) {
			return false;
		}
		String cpf = numcpf.toString();
		cpf = formatService.fillLeft(cpf.toString(), Constants.ZERO_STRING,
				Constants.CPF_SIZE);
		if (cpf.length() != Constants.CPF_SIZE) {
			return false;
		}
		Integer digito1 = calcularDigito(cpf.substring(0, 9), pesoCPF);
		Integer digito2 = calcularDigito(cpf.substring(0, 9) + digito1, pesoCPF);
		return cpf.equals(cpf.substring(0, 9) + digito1.toString()
		+ digito2.toString());
	}

	@Override
	public boolean isValidCNPJ(Long numcnpj) {
		if (numcnpj == null) {
			return false;
		}
		String cnpj = numcnpj.toString();
		cnpj = formatService.fillLeft(cnpj.toString(), Constants.ZERO_STRING,
				Constants.CNPJ_SIZE);
		if (cnpj.length() != Constants.CNPJ_SIZE) {
			return false;
		}
		Integer digito1 = calcularDigito(cnpj.substring(0, 12), pesoCNPJ);
		Integer digito2 = calcularDigito(cnpj.substring(0, 12) + digito1,
				pesoCNPJ);
		return cnpj.equals(cnpj.substring(0, 12) + digito1.toString()
		+ digito2.toString());
	}

	@Override
	public boolean isValidEmail(String value) {
		if (value == null) {
			return false;
		} else {
			return email_pattern.matcher(value.toString()).matches();
		}
	}

	@Override
	public boolean isValidCelular(Integer ddd, Integer celular) {
		String strCelular = ddd.toString() + celular.toString();
		if (strCelular.length() == Constants.DDD_CELULAR_LENGHT) {
			return celular9_pattern.matcher(strCelular).matches();
		} else if (strCelular.length() == Constants.DDD_TELEFONE_LENGHT) {
			return celular8_pattern.matcher(strCelular).matches();
		} else {
			return false;
		}
	}

	@Override
	public boolean isValidTelefone(Integer ddd, Integer telefone) {
		String strTelefone = ddd.toString() + telefone.toString();
		if (strTelefone.length() == Constants.DDD_TELEFONE_LENGHT) {
			return telefone_pattern.matcher(strTelefone).matches();
		} else {
			return false;
		}
	}

	@Override
	public boolean isValidPassword(String password) {
		return password_pattern.matcher(password).matches();
	}

	@Override
	public boolean confirmPassword(String password, String confirm) {
		if (password.equals(confirm)) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public String encryptPassword(String pass) throws NoSuchAlgorithmException {
		md = MessageDigest.getInstance("MD5");
		if (pass == null || pass.length() == 0) {
			throw new IllegalArgumentException(
					"String to encript cannot be null or zero length");
		}

		md.update(pass.getBytes());
		byte[] hash = md.digest();
		StringBuffer hexString = new StringBuffer();
		for (int i = 0; i < hash.length; i++) {
			if ((0xff & hash[i]) < 0x10) {
				hexString.append("0" + Integer.toHexString((0xFF & hash[i])));
			} else {
				hexString.append(Integer.toHexString(0xFF & hash[i]));
			}
		}
		return hexString.toString();
	}

	@Override
	public boolean isClienteCompletlyRegistered(Cliente cliente) {
		if (cliente == null || cliente.getStremail() == null || cliente.getStremail().isEmpty() 
				|| cliente.getStrnome() == null || cliente.getStrnome().isEmpty()) {
			return false;
		} else {
			return true;
		}
	}
}
