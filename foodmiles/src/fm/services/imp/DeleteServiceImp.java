package fm.services.imp;

import javax.annotation.PostConstruct;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

import fm.services.DeleteService;

@ApplicationScoped
@ManagedBean(eager = true)
public class DeleteServiceImp extends GenericServiceImp implements
		DeleteService {
	@Override
	@PostConstruct
	public void init() {
	}

}