package fm.converters;

import java.text.ParseException;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import fm.constants.Constants;
import fm.managed.bean.imp.GenericManagedBeanImp;
import fm.services.FormatService;

@FacesConverter(value = "BooleanConverter")
public class BooleanConverter extends GenericManagedBeanImp implements
		Converter {
	FormatService formatService;

	public BooleanConverter() {
		formatService = super.findBean(FormatService.class);
	}

	@Override
	public Object getAsObject(FacesContext context, UIComponent component,
			String value) {
		return null;

	}

	@Override
	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		try {
			return formatService.formatBooleanToString((Boolean) value);
		} catch (ParseException e) {
			return Constants.EMPTY_STRING;
		}
	}
}